/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-sevone',
      type: 'SevOne',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const SevOne = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] SevOne Adapter Test', () => {
  describe('SevOne Class Tests', () => {
    const a = new SevOne(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */
    // define data used throughout the test
    const randExt = `${Math.random()}`;
    const now = new Date().getTime();

    let deviceId = 1245;
    const deviceNm = `test${randExt}`;
    const deviceIP = '10.10.10.1';
    const device = { name: deviceNm, ipAddress: deviceIP };
    const deviceFilterObj = { disablePolling: false };
    const udevice = {
      id: deviceId, name: deviceNm, ipAddress: deviceIP, description: 'changed description'
    };

    let deviceGrpId = 2;
    const deviceGrpNm = `test grp${randExt}`;
    const deviceGrp = { name: deviceGrpNm, parentId: 2 };
    const udeviceGrp = { id: deviceGrpId, name: `changed ${deviceGrpNm}`, parentId: 2 };

    let deviceCompId = 1234;
    let deviceCompNm = `test comp${randExt}`;
    const deviceCompFilterObj = { isEnabled: true };
    const deviceComp = {
      name: deviceCompNm,
      description: 'test blah',
      pluginId: 12,
      pluginObjectTypeId: 9
    };
    const udeviceComp = {
      name: deviceCompNm,
      description: 'changed description of blah',
      pluginId: 1,
      pluginObjectTypeId: 1,
      indicators: [
        {
          id: 1,
          evaluationOrder: 1,
          format: 'GAUGE',
          isBaselining: true,
          lastInvalidationTime: 1,
          maxValue: 1,
          pluginIndicatorTypeId: 1,
          syntheticExpression: 'string',
          extendedInfo: {},
          isDeleted: true,
          isEnabled: true
        }
      ]
    };

    let componentGrpId = 3;
    const componentGrpNm = `test grp${randExt}`;
    const componentGrp = { name: componentGrpNm, parentId: 4 };
    const ucomponentGrp = { id: componentGrpId, name: `changed ${componentGrpNm}`, parentId: 4 };

    const indicatorId = (stub) ? 9876 : 1;
    const indicator = { deviceId, componentId: deviceCompId, indicatorData: [{ indicatorId, value: 5 }] };

    let mapId = 1;
    let mapNm = `test map${randExt}`;
    const mapObj = { name: mapNm, imageId: 11 };
    const umapObj = { id: mapId, name: `changed ${mapNm}`, imageId: 11 };

    let mapNodeAId = 1;
    let mapNodeANm = `test map A${randExt}`;
    let mapNodeBId = 1;
    let mapNodeBNm = `test map B${randExt}`;
    const mapNodeAObj = {
      name: mapNodeANm, type: 'Device', x: 1, y: 1, elements: [{ deviceId, elementId: 1 }]
    };
    const mapNodeBObj = {
      name: mapNodeBNm, type: 'Device', x: 1, y: 1, elements: [{ elementId: 1 }]
    };
    const umapNodeObj = {
      id: mapNodeAId, name: `changed ${mapNodeANm}`, type: 'Device', x: 1, y: 1, elements: [{ deviceId, elementId: 1 }]
    };

    let mapConnId = 1;
    const mapConnObj = {
      nodeASide: mapNodeAId, nodeBSide: mapNodeBId, type: 'Device', elements: [{ elementId: 1 }]
    };
    const umapConnObj = {
      id: mapConnId, nodeASide: mapNodeAId, nodeBSide: mapNodeBId, type: 'Object', elements: [{ elementId: 1, deviceId, pluginId: 4 }]
    };

    let alertId = 1;
    const alertMsg = `failed blah${randExt}`;
    const alertOrigin = 'trap';
    let alertCreated = false;
    let getAlertCount = 0;
    const alertFilterObj = { message: alertMsg };
    const alertObj = {
      message: alertMsg, origin: alertOrigin, deviceId, componentId: deviceCompId, severity: 3, startTime: now
    };

    let updAlertCount = 0;
    const ualertObj = {
      id: alertId, message: `changed ${alertMsg}`, origin: alertOrigin, deviceId
    };

    /**
     * Runs the common asserts for alert counts in stub mode
     */
    function runAlertCountAsserts(data, ccnt, ecnt, acnt) {
      assert.equal('critical', data.response.counts[2].name);
      assert.equal(ccnt, data.response.counts[2].count);
      assert.equal('error', data.response.counts[3].name);
      assert.equal(ecnt, data.response.counts[3].count);
      assert.equal(acnt, data.response.alerts.length);
      assert.equal(alertId, data.response.alerts[0].id);
    }

    describe('#createDevice', () => {
      it('should create a new device', (done) => {
        try {
          a.createDevice(device, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual('', data.response.id);
              }

              deviceId = data.response.id;
              umapConnObj.elements = [{ elementId: 1, deviceId, pluginId: 4 }];
              alertObj.deviceId = data.response.id;
              ualertObj.deviceId = data.response.id;
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevices', () => {
      it('should get the devices', (done) => {
        try {
          a.getDevices(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2, data.response.length);
                assert.equal(deviceId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device by id', (done) => {
        try {
          a.getDevices(deviceId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceId, data.response.id);
              } else {
                assert.equal(deviceId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device by name', (done) => {
        try {
          a.getDevices(null, deviceNm, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2, data.response.length);
                assert.equal(deviceId, data.response[0].id);
              } else {
                assert.equal(deviceId, data.response[0].id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device by ip', (done) => {
        try {
          a.getDevices(null, null, deviceIP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2, data.response.length);
                assert.equal(deviceId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device error', (done) => {
        try {
          a.getDevices('error', null, null, null, (data, error) => {
            try {
              const displayE = 'Error 400 received on request';
              runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesById', () => {
      it('should get the devices by id', (done) => {
        try {
          a.getDevicesById(deviceId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceId, data.response.id);
              } else {
                assert.equal(deviceId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesByName', () => {
      it('should get the devices by name', (done) => {
        try {
          a.getDevicesByName(deviceNm, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2, data.response.length);
                assert.equal(deviceId, data.response[0].id);
              } else {
                assert.equal(deviceId, data.response[0].id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesByIp', () => {
      it('should get the devices by ip', (done) => {
        try {
          a.getDevicesByIp(deviceIP, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2, data.response.length);
                assert.equal(deviceId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSevDevicesFiltered', () => {
      it('should get the filtered devices', (done) => {
        try {
          a.getSevDevicesFiltered(deviceFilterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2, data.response.length);
                assert.equal(deviceId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesForNameorIP', () => {
      it('should get the device by name', (done) => {
        try {
          a.getDevicesForNameorIP(deviceNm, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2, data.response.length);
                assert.equal(deviceId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get the device by ip', (done) => {
        try {
          a.getDevicesForNameorIP(null, deviceIP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2, data.response.length);
                assert.equal(deviceId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDevice', () => {
      it('should update a device', (done) => {
        try {
          a.updateDevice(deviceId, udevice, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceGroup', () => {
      it('should create a new device group', (done) => {
        try {
          a.createDeviceGroup(deviceGrp, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceGrpId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual('', data.response.id);
              }

              deviceGrpId = data.response.id;
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDeviceToGroup', () => {
      it('should add a device to a group', (done) => {
        try {
          a.addDeviceToGroup(deviceId, deviceGrpId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroups', () => {
      it('should get the device groups', (done) => {
        try {
          a.getDeviceGroups(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(5, data.response.length);
                assert.equal(deviceGrpId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device group by id', (done) => {
        try {
          a.getDeviceGroups(deviceGrpId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceGrpId, data.response.id);
              } else {
                assert.equal(deviceGrpId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupsById', () => {
      it('should get device group by id', (done) => {
        try {
          a.getDeviceGroupsById(deviceGrpId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceGrpId, data.response.id);
              } else {
                assert.equal(deviceGrpId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceGroup', () => {
      it('should update a device group', (done) => {
        try {
          a.updateDeviceGroup(deviceGrpId, udeviceGrp, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceComponent', () => {
      it('should create a new device component', (done) => {
        try {
          a.createDeviceComponent(deviceId, deviceComp, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceCompId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual('', data.response.id);
              }

              deviceCompId = data.response.id;
              deviceCompNm = data.response.name;
              alertObj.componentId = data.response.id;
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceComponents', () => {
      it('should get the device components', (done) => {
        try {
          a.getDeviceComponents(1, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.length);
                assert.equal(deviceCompId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device component by id', (done) => {
        try {
          a.getDeviceComponents(deviceId, null, deviceCompId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceCompId, data.response.id);
              } else {
                assert.equal(deviceCompId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device component by id/name', (done) => {
        try {
          a.getDeviceComponents(deviceId, null, null, deviceCompNm, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceCompId, data.response.id);
              } else {
                assert.equal(deviceCompId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device component by id/name - not able to find', (done) => {
        try {
          a.getDeviceComponents(deviceId, null, null, 'BadComponent', null, (data, error) => {
            try {
              const displayE = `Could not find component BadComponent on device ${deviceId}`;
              runErrorAsserts(data, error, 'AD.999', 'Test-sevone-adapter-getDeviceComponents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device component by name/id', (done) => {
        try {
          a.getDeviceComponents(null, deviceNm, deviceCompId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceCompId, data.response.id);
              } else {
                assert.equal(deviceCompId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device component by name', (done) => {
        try {
          a.getDeviceComponents(null, deviceNm, null, deviceCompNm, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceCompId, data.response.id);
              } else {
                assert.equal(deviceCompId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device component by name - not able to find', (done) => {
        try {
          a.getDeviceComponents(null, deviceNm, null, 'BadComponent', null, (data, error) => {
            try {
              const displayE = `Could not find component BadComponent on device ${deviceNm}`;
              runErrorAsserts(data, error, 'AD.999', 'Test-sevone-adapter-getDeviceComponents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceComponentsById', () => {
      it('should get device component by id', (done) => {
        try {
          a.getDeviceComponentsById(deviceId, deviceCompId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceCompId, data.response.id);
              } else {
                assert.equal(deviceCompId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceComponentsByName', () => {
      it('should get device component by name', (done) => {
        try {
          a.getDeviceComponentsByName(deviceNm, deviceCompNm, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceCompId, data.response.id);
              } else {
                assert.equal(deviceCompId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceComponentsFiltered', () => {
      it('should get the filtered device components', (done) => {
        try {
          a.getDeviceComponentsFiltered(deviceCompFilterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.length);
                assert.equal(deviceCompId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceComponentId', () => {
      it('should get the device component id - by names', (done) => {
        try {
          a.getDeviceComponentId(null, deviceNm, deviceCompNm, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceCompId, data.response.id);
              } else {
                assert.equal(deviceCompId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device component - by names - not able to find', (done) => {
        try {
          a.getDeviceComponentId(null, deviceNm, 'BadComponent', null, (data, error) => {
            try {
              const displayE = `Could not find component BadComponent on device ${deviceNm}`;
              runErrorAsserts(data, error, 'AD.999', 'Test-sevone-adapter-getDeviceComponentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get the device components - by id/name', (done) => {
        try {
          a.getDeviceComponentId(deviceId, null, deviceCompNm, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(deviceCompId, data.response.id);
              } else {
                assert.equal(deviceCompId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get device component - by id/name - not able to find', (done) => {
        try {
          a.getDeviceComponentId(deviceId, null, 'BadComponent', null, (data, error) => {
            try {
              const displayE = `Could not find component BadComponent on device ${deviceId}`;
              runErrorAsserts(data, error, 'AD.999', 'Test-sevone-adapter-getDeviceComponentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceComponent', () => {
      it('should update a device component', (done) => {
        try {
          a.updateDeviceComponent(deviceId, deviceCompId, udeviceComp, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createComponentGroup', () => {
      it('should create a new component group', (done) => {
        try {
          a.createComponentGroup(componentGrp, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(componentGrpId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual('', data.response.id);
              }

              componentGrpId = data.response.id;
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDeviceComponentToGroup', () => {
      it('should add a device component to a group', (done) => {
        try {
          a.addDeviceComponentToGroup(deviceId, deviceCompId, componentGrpId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComponentGroups', () => {
      it('should get the component groups', (done) => {
        try {
          a.getComponentGroups(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(20, data.response.length);
                assert.equal(componentGrpId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get component group by id', (done) => {
        try {
          a.getComponentGroups(componentGrpId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(componentGrpId, data.response.id);
              } else {
                assert.equal(componentGrpId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComponentGroupsById', () => {
      it('should get component group by id', (done) => {
        try {
          a.getComponentGroupsById(componentGrpId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(componentGrpId, data.response.id);
              } else {
                assert.equal(componentGrpId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateComponentGroup', () => {
      it('should update a component group', (done) => {
        try {
          a.updateComponentGroup(componentGrpId, ucomponentGrp, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIndicatorData', () => {
      it('should create indicator data', (done) => {
        try {
          a.createIndicatorData(indicator, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // NEED TO ONLY RUNIN STUB MODE AS INDICATOR IS NOT ON COMPONENT PROPERLY
    // NOT SURE IF SEVONE ISSUE OR TESTING DATA ISSUE
    if (stub) {
      describe('#getIndicators', () => {
        it('should get the indicators', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicators(deviceId, null, deviceCompId, null, null, null, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.length);
                assert.equal(indicatorId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get indicator - by ids', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicators(deviceId, null, deviceCompId, null, indicatorId, null, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(indicatorId, data.response.id);
              } else {
                assert.equal(indicatorId, data.response.id);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get indicator - by id/name', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicators(deviceId, null, null, deviceCompNm, indicatorId, null, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(indicatorId, data.response.id);
              } else {
                assert.equal(indicatorId, data.response.id);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get indicator - by id/name - not able to find', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicators(deviceId, null, null, 'BadComponent', indicatorId, null, (data, error) => {
              const displayE = `Could not find component BadComponent on device ${deviceId}`;
              runErrorAsserts(data, error, 'AD.999', 'Test-sevone-adapter-getDeviceComponents', displayE);
              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get indicator - by name/id', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicators(null, deviceNm, deviceCompId, null, indicatorId, null, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(indicatorId, data.response.id);
              } else {
                assert.equal(indicatorId, data.response.id);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get indicator - by names', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicators(null, deviceNm, null, deviceCompNm, indicatorId, null, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(indicatorId, data.response.id);
              } else {
                assert.equal(indicatorId, data.response.id);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get indicator - by names - not able to find', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicators(null, deviceNm, null, 'BadComponent', indicatorId, null, (data, error) => {
              const displayE = `Could not find component BadComponent on device ${deviceNm}`;
              runErrorAsserts(data, error, 'AD.999', 'Test-sevone-adapter-getDeviceComponents', displayE);
              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      });

      describe('#getIndicatorsById', () => {
        it('should get indicator - by ids', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorsById(deviceId, deviceCompId, indicatorId, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(indicatorId, data.response.id);
              } else {
                assert.equal(indicatorId, data.response.id);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      });

      describe('#getIndicatorsByName', () => {
        it('should get indicator - by names', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorsByName(deviceNm, deviceCompNm, indicatorId, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(indicatorId, data.response.id);
              } else {
                assert.equal(indicatorId, data.response.id);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      });

      describe('#getIndicatorData', () => {
        it('should get data for the indicator - by ids', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorData(deviceId, null, deviceCompId, null, indicatorId, null, null, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.notEqual(0, data.response.data.length);
                assert.equal(1, data.response.data[0].value);
              } else {
                assert.notEqual(0, data.response.data.length);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get data for the indicator - by id/name', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorData(deviceId, null, null, deviceCompNm, indicatorId, null, null, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.notEqual(0, data.response.data.length);
                assert.equal(1, data.response.data[0].value);
              } else {
                assert.notEqual(0, data.response.data.length);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get data for the indicator - by id/name - not able to find', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorData(deviceId, null, null, 'BadComponent', indicatorId, null, null, (data, error) => {
              const displayE = `Could not find component BadComponent on device ${deviceId}`;
              runErrorAsserts(data, error, 'AD.999', 'Test-sevone-adapter-getDeviceComponents', displayE);
              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get data for the indicator - by name/id', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorData(null, deviceNm, deviceCompId, null, indicatorId, null, null, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.notEqual(0, data.response.data.length);
                assert.equal(1, data.response.data[0].value);
              } else {
                assert.notEqual(0, data.response.data.length);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get data for the indicator - by names', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorData(null, deviceNm, null, deviceCompNm, indicatorId, null, null, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.notEqual(0, data.response.data.length);
                assert.equal(1, data.response.data[0].value);
              } else {
                assert.notEqual(0, data.response.data.length);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get data for the indicator - by names - not able to find', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorData(null, deviceNm, null, 'BadComponent', indicatorId, null, null, (data, error) => {
              const displayE = `Could not find component BadComponent on device ${deviceNm}`;
              runErrorAsserts(data, error, 'AD.999', 'Test-sevone-adapter-getDeviceComponents', displayE);
              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get data for the indicator for given range', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorData(deviceId, null, deviceCompId, null, indicatorId, now - 86400000, now, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.notEqual(0, data.response.data.length);
                assert.equal(1, data.response.data[0].value);
              } else {
                assert.notEqual(0, data.response.data.length);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      });

      describe('#getIndicatorDataWithRangeById', () => {
        it('should get data for the indicator - by ids', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorDataWithRangeById(deviceId, deviceCompId, indicatorId, null, null, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.notEqual(0, data.response.data.length);
                assert.equal(1, data.response.data[0].value);
              } else {
                assert.notEqual(0, data.response.data.length);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get data for the indicator for given range', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorDataWithRangeById(deviceId, deviceCompId, indicatorId, now - 86400000, now, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.notEqual(0, data.response.data.length);
                assert.equal(1, data.response.data[0].value);
              } else {
                assert.notEqual(0, data.response.data.length);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      });

      describe('#getIndicatorDataWithRangeByName', () => {
        it('should get data for the indicator - by names', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorDataWithRangeByName(deviceNm, deviceCompNm, indicatorId, null, null, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.notEqual(0, data.response.data.length);
                assert.equal(1, data.response.data[0].value);
              } else {
                assert.notEqual(0, data.response.data.length);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
        it('should get data for the indicator for given range', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorDataWithRangeByName(deviceNm, deviceCompNm, indicatorId, now - 86400000, now, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.notEqual(0, data.response.data.length);
                assert.equal(1, data.response.data[0].value);
              } else {
                assert.notEqual(0, data.response.data.length);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      });

      describe('#getIndicatorDataForDayById', () => {
        it('should get data for the indicator - by ids', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorDataForDayById(deviceId, deviceCompId, indicatorId, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.notEqual(0, data.response.data.length);
                assert.equal(1, data.response.data[0].value);
              } else {
                assert.notEqual(0, data.response.data.length);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      });

      describe('#getIndicatorDataForDayByName', () => {
        it('should get data for the indicator - by names', (done) => {
          const p = new Promise((resolve) => {
            a.getIndicatorDataForDayByName(deviceNm, deviceCompNm, indicatorId, (data, error) => {
              runCommonAsserts(data, error);

              if (stub) {
                assert.notEqual(0, data.response.data.length);
                assert.equal(1, data.response.data[0].value);
              } else {
                assert.notEqual(0, data.response.data.length);
              }

              resolve(data);
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      });
    }

    describe('#createMap', () => {
      it('should create a new map', (done) => {
        try {
          a.createMap(mapObj, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(mapId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual('', data.response.id);
              }

              mapId = data.response.id;
              mapNm = data.response.name;
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMaps', () => {
      it('should get the maps', (done) => {
        try {
          a.getMaps(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(5, data.response.length);
                assert.equal(mapId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get map by id', (done) => {
        try {
          a.getMaps(mapId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(mapId, data.response.id);
              } else {
                assert.equal(mapId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapsById', () => {
      it('should get map by id', (done) => {
        try {
          a.getMapsById(mapId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(mapId, data.response.id);
              } else {
                assert.equal(mapId, data.response.id);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMap', () => {
      it('should update a map', (done) => {
        try {
          a.updateMap(mapId, umapObj, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMapNode', () => {
      it('should create a new map node - A', (done) => {
        try {
          a.createMapNode(mapId, mapNodeAObj, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(mapNodeAId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual('', data.response.id);
              }

              mapNodeAId = data.response.id;
              mapNodeANm = data.response.name;
              mapConnObj.nodeASide = data.response.id;
              umapConnObj.nodeASide = data.response.id;
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should create a new map node - B', (done) => {
        try {
          a.createMapNode(mapId, mapNodeBObj, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(mapNodeBId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual('', data.response.id);
              }

              mapNodeBId = data.response.id;
              mapNodeBNm = data.response.name;
              mapConnObj.nodeBSide = data.response.id;
              umapConnObj.nodeBSide = data.response.id;
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapNodes', () => {
      it('should get the map nodes', (done) => {
        try {
          a.getMapNodes(mapId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.length);
                assert.equal(mapNodeAId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapNodesById', () => {
      it('should get the map nodes by id', (done) => {
        try {
          a.getMapNodesById(mapId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.length);
                assert.equal(mapNodeAId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMapNode', () => {
      it('should update a map node', (done) => {
        try {
          a.updateMapNode(mapId, mapNodeAId, umapNodeObj, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMapConnection', () => {
      it('should create a new map connection', (done) => {
        try {
          a.createMapConnection(mapId, mapConnObj, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(mapConnId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual('', data.response.id);
              }

              mapConnId = data.response.id;
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapConnections', () => {
      it('should get the map connections', (done) => {
        try {
          a.getMapConnections(mapId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.length);
                assert.equal(mapConnId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapConnectionsById', () => {
      it('should get the map connections by id', (done) => {
        try {
          a.getMapConnectionsById(mapId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.length);
                assert.equal(mapConnId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMapConnection', () => {
      it('should update a map connection', (done) => {
        try {
          a.updateMapConnection(mapId, mapConnId, umapConnObj, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAlert', () => {
      it('should create a new alert', (done) => {
        try {
          a.createAlert(alertObj, (data, error) => {
            try {
              runCommonAsserts(data, error);
              assert.equal('success', data.response);

              const createWait = setTimeout(() => {
                alertCreated = true;
                clearTimeout(createWait);
                done();
              }, 10000);
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlerts', () => {
      it('should get the alerts', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlerts(null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  alertId = data.response[0].id;
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alert by id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlerts(alertId, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(alertId, data.response.id);
                  } else {
                    assert.equal(alertId, data.response.id);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsById', () => {
      it('should get alert by id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsById(alertId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(alertId, data.response.id);
                  } else {
                    assert.equal(alertId, data.response.id);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsFiltered', () => {
      it('should get the filtered alerts', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsFiltered(alertFilterObj, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDevice', () => {
      it('should get alerts for device - by id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDevice(deviceId, null, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  alertId = data.response[0].id;
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts for device - by name', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDevice(null, deviceNm, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts for device - by ip', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDevice(null, null, deviceIP, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceById', () => {
      it('should get alerts for device - by id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceById(deviceId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceByName', () => {
      it('should get alerts for device - by name', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceByName(deviceNm, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceByIp', () => {
      it('should get alerts for device - by ip', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceByIp(deviceIP, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDevice', () => {
      it('should get alert counts for device - by id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForDevice(deviceId, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alert counts for device - by name', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForDevice(null, deviceNm, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alert counts for device - by ip', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForDevice(null, null, deviceIP, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceById', () => {
      it('should get alert counts for device - by id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForDeviceById(deviceId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceByName', () => {
      it('should get alert counts for device - by name', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForDeviceByName(deviceNm, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceByIp', () => {
      it('should get alert counts for device - by ip', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForDeviceByIp(deviceIP, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    let endTime = new Date().getTime();

    describe('#getAlertsForDeviceWithRange', () => {
      it('should get alerts within range for device - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceWithRange(deviceId, null, null, null, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts within range for device - start time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceWithRange(deviceId, null, null, now - 86400000, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts within range for device - end time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          endTime = new Date().getTime();
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceWithRange(deviceId, null, null, null, endTime, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts within range for device - range', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceWithRange(deviceId, null, null, now - 86400000, endTime, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts within range for device - by name', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceWithRange(null, deviceNm, null, null, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts within range for device - by ip', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceWithRange(null, null, deviceIP, null, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceWithRangeById', () => {
      it('should get alerts within range for device by id - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceWithRangeById(deviceId, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceWithRangeByName', () => {
      it('should get alerts within range for device by name - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceWithRangeByName(deviceNm, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceWithRangeByIp', () => {
      it('should get alerts within range for device by ip - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceWithRangeByIp(deviceIP, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceForDayById', () => {
      it('should get alerts for day for device by id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceForDayById(deviceId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceForDayByName', () => {
      it('should get alerts for day for device by name', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceForDayByName(deviceNm, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceForDayByIp', () => {
      it('should get alerts for day for device by ip', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceForDayByIp(deviceIP, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceGroup', () => {
      it('should get alerts for device group', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceGroup(deviceGrpId, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceGroupById', () => {
      it('should get alerts for device group by id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceGroupById(deviceGrpId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceGroup', () => {
      it('should get alert counts for device group', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForDeviceGroup(deviceGrpId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceGroupById', () => {
      it('should get alert counts for device group by id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForDeviceGroupById(deviceGrpId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceGroupWithRange', () => {
      it('get alerts within range for device group - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceGroupWithRange(deviceGrpId, null, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for device group - start time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceGroupWithRange(deviceGrpId, now - 86400000, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for device group - end time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceGroupWithRange(deviceGrpId, null, endTime, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts within range for device group - range', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceGroupWithRange(deviceGrpId, now - 86400000, endTime, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceGroupWithRangeById', () => {
      it('get alerts within range for device group by id - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceGroupWithRangeById(deviceGrpId, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceGroupForDayById', () => {
      it('get alerts for day for device group by id - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceGroupForDayById(deviceGrpId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponent', () => {
      it('should get alerts for device component - by ids', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponent(deviceId, null, deviceCompId, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts for device component - by id/name', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponent(deviceId, null, null, deviceCompNm, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts for device component - by id/name - not able to find', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponent(deviceId, null, null, 'BadComponent', null, (data, error) => {
                try {
                  getAlertCount += 1;
                  const displayE = `Could not find component BadComponent on device ${deviceId}`;
                  runErrorAsserts(data, error, 'AD.999', 'Test-sevone-adapter-getDeviceComponents', displayE);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts for device component - by name/id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponent(null, deviceNm, deviceCompId, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts for device component - by names', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponent(null, deviceNm, null, deviceCompNm, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alerts for device component - by names - not able to find', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponent(null, deviceNm, null, 'BadComponent', null, (data, error) => {
                try {
                  getAlertCount += 1;
                  const displayE = `Could not find component BadComponent on device ${deviceNm}`;
                  runErrorAsserts(data, error, 'AD.999', 'Test-sevone-adapter-getDeviceComponents', displayE);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentById', () => {
      it('should get alerts for device component - by ids', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentById(deviceId, deviceCompId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentByName', () => {
      it('should get alerts for device component - by names', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentByName(deviceNm, deviceCompNm, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceComponent', () => {
      it('should get alert counts for device component - by ids', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForDeviceComponent(deviceId, null, deviceCompId, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alert counts for device component - by id/name', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              let compNm = deviceCompNm;
              if (stub) {
                compNm = 'string';
              }
              a.getAlertCountsForDeviceComponent(deviceId, null, null, compNm, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alert counts for device component - by name/id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForDeviceComponent(null, deviceNm, deviceCompId, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should get alert counts for device component - by names', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              let compNm = deviceCompNm;
              if (stub) {
                compNm = 'string';
              }
              a.getAlertCountsForDeviceComponent(null, deviceNm, null, compNm, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceComponentById', () => {
      it('should get alert counts for device component - by ids', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForDeviceComponentById(deviceId, deviceCompId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForDeviceComponentByName', () => {
      it('should get alert counts for device component - by names', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForDeviceComponentByName(deviceNm, deviceCompNm, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentWithRange', () => {
      it('get alerts within range for device component - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentWithRange(deviceId, null, deviceCompId, null, null, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for dev component - start time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentWithRange(deviceId, null, deviceCompId, null, now - 86400000, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for device component - end time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentWithRange(deviceId, null, deviceCompId, null, null, endTime, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for device component - range', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentWithRange(deviceId, null, deviceCompId, null, now - 86400000, endTime, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for device component - by id/name', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentWithRange(deviceId, null, null, deviceCompNm, null, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for device component - by id/name - not able to find', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentWithRange(deviceId, null, null, 'BadComponent', null, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  const displayE = `Could not find component BadComponent on device ${deviceId}`;
                  runErrorAsserts(data, error, 'AD.999', 'Test-sevone-adapter-getDeviceComponents', displayE);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for device component - by name/id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentWithRange(null, deviceNm, deviceCompId, null, null, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for device component - by names', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentWithRange(null, deviceNm, null, deviceCompNm, null, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for device component - by names - not able to find', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentWithRange(null, deviceNm, null, 'BadComponent', null, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  const displayE = `Could not find component BadComponent on device ${deviceNm}`;
                  runErrorAsserts(data, error, 'AD.999', 'Test-sevone-adapter-getDeviceComponents', displayE);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentWithRangeById', () => {
      it('get alerts within range for device component by id - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentWithRangeById(deviceId, deviceCompId, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentWithRangeByName', () => {
      it('get alerts within range for device component by name - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentWithRangeByName(deviceNm, deviceCompNm, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentForDayById', () => {
      it('get alerts for day for device component by id - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentForDayById(deviceId, deviceCompId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForDeviceComponentForDayByName', () => {
      it('get alerts for day for device component by name - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForDeviceComponentForDayByName(deviceNm, deviceCompNm, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForComponentGroup', () => {
      it('should get alerts for component group', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForComponentGroup(componentGrpId, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForComponentGroupById', () => {
      it('should get alerts for component group by id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForComponentGroupById(componentGrpId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForComponentGroup', () => {
      it('should get alert counts for component group', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForComponentGroup(componentGrpId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertCountsForComponentGroupById', () => {
      it('should get alert counts for component group by id', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertCountsForComponentGroupById(componentGrpId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForComponentGroupWithRange', () => {
      it('get alerts within range for component group - no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForComponentGroupWithRange(componentGrpId, null, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for component grp - start time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForComponentGroupWithRange(componentGrpId, now - 86400000, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for component group - end time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForComponentGroupWithRange(componentGrpId, null, endTime, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('get alerts within range for component group - range', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForComponentGroupWithRange(componentGrpId, now - 86400000, endTime, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForComponentGroupWithRangeById', () => {
      it('get alerts within range for component group by id- no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForComponentGroupWithRangeById(componentGrpId, null, null, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForComponentGroupForDayById', () => {
      it('get alerts for day for component group by id- no time', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (alertCreated && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.getAlertsForComponentGroupForDayById(componentGrpId, (data, error) => {
                try {
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    let getFinalAlertCount = 75;
    // NEED TO ONLY RUN IN STUB MODE AS MAP NODE AND COMPONENT ARE NOT ON MAP PROPERLY
    // NOT SURE IF SEVONE ISSUE OR TESTING DATA ISSUE
    if (stub) {
      getFinalAlertCount = 87;
      describe('#getAlertsForMapConnection', () => {
        it('should get alerts for map connection', (done) => {
          let running = false;
          const interval = setInterval(() => {
            if (alertCreated && !running) {
              running = true;
              clearInterval(interval);
              const p = new Promise((resolve) => {
                a.getAlertsForMapConnection(mapId, mapConnId, null, (data, error) => {
                  resolve(data);
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                });
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            }
          }, 1000);
        }).timeout(attemptTimeout);
      });

      describe('#getAlertsForMapConnectionById', () => {
        it('should get alerts for map connection by id', (done) => {
          let running = false;
          const interval = setInterval(() => {
            if (alertCreated && !running) {
              running = true;
              clearInterval(interval);
              const p = new Promise((resolve) => {
                a.getAlertsForMapConnectionById(mapId, mapConnId, (data, error) => {
                  resolve(data);
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                });
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            }
          }, 1000);
        }).timeout(attemptTimeout);
      });

      describe('#getAlertCountsForMapConnection', () => {
        it('should get alert counts for map connections', (done) => {
          let running = false;
          const interval = setInterval(() => {
            if (alertCreated && !running) {
              running = true;
              clearInterval(interval);
              const p = new Promise((resolve) => {
                a.getAlertCountsForMapConnection(mapId, mapConnId, (data, error) => {
                  resolve(data);
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                });
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            }
          }, 1000);
        }).timeout(attemptTimeout);
      });

      describe('#getAlertCountsForMapConnectionById', () => {
        it('should get alert counts for map connections by id', (done) => {
          let running = false;
          const interval = setInterval(() => {
            if (alertCreated && !running) {
              running = true;
              clearInterval(interval);
              const p = new Promise((resolve) => {
                a.getAlertCountsForMapConnectionById(mapId, mapConnId, (data, error) => {
                  resolve(data);
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                });
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            }
          }, 1000);
        }).timeout(attemptTimeout);
      });

      describe('#getAlertsForMapNode', () => {
        it('should get alerts for map nodes', (done) => {
          let running = false;
          const interval = setInterval(() => {
            if (alertCreated && !running) {
              running = true;
              clearInterval(interval);
              const p = new Promise((resolve) => {
                a.getAlertsForMapNode(mapId, mapNodeAId, null, (data, error) => {
                  resolve(data);
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                });
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            }
          }, 1000);
        }).timeout(attemptTimeout);
      });

      describe('#getAlertsForMapNodeById', () => {
        it('should get alerts for map nodes by id', (done) => {
          let running = false;
          const interval = setInterval(() => {
            if (alertCreated && !running) {
              running = true;
              clearInterval(interval);
              const p = new Promise((resolve) => {
                a.getAlertsForMapNodeById(mapId, mapNodeAId, (data, error) => {
                  resolve(data);
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(2, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                });
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            }
          }, 1000);
        }).timeout(attemptTimeout);
      });

      describe('#getAlertCountsForMapNode', () => {
        it('should get alert counts for map nodes', (done) => {
          let running = false;
          const interval = setInterval(() => {
            if (alertCreated && !running) {
              running = true;
              clearInterval(interval);
              const p = new Promise((resolve) => {
                a.getAlertCountsForMapNode(mapId, mapNodeAId, (data, error) => {
                  resolve(data);
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                });
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            }
          }, 1000);
        }).timeout(attemptTimeout);
      });

      describe('#getAlertCountsForMapNodeById', () => {
        it('should get alert counts for map nodes by id', (done) => {
          let running = false;
          const interval = setInterval(() => {
            if (alertCreated && !running) {
              running = true;
              clearInterval(interval);
              const p = new Promise((resolve) => {
                a.getAlertCountsForMapNodeById(mapId, mapNodeAId, (data, error) => {
                  resolve(data);
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 1, 1, 2);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                });
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            }
          }, 1000);
        }).timeout(attemptTimeout);
      });

      describe('#getAlertsForMap', () => {
        it('should get alerts for maps', (done) => {
          let running = false;
          const interval = setInterval(() => {
            if (alertCreated && !running) {
              running = true;
              clearInterval(interval);
              const p = new Promise((resolve) => {
                a.getAlertsForMap(mapId, null, (data, error) => {
                  resolve(data);
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(4, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                });
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            }
          }, 1000);
        }).timeout(attemptTimeout);
      });

      describe('#getAlertsForMapById', () => {
        it('should get alerts for maps by id', (done) => {
          let running = false;
          const interval = setInterval(() => {
            if (alertCreated && !running) {
              running = true;
              clearInterval(interval);
              const p = new Promise((resolve) => {
                a.getAlertsForMapById(mapId, (data, error) => {
                  resolve(data);
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    assert.equal(4, data.response.length);
                    assert.equal(alertId, data.response[0].id);
                  } else {
                    assert.notEqual(0, data.response.length);
                  }

                  done();
                });
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            }
          }, 1000);
        }).timeout(attemptTimeout);
      });

      describe('#getAlertCountsForMap', () => {
        it('should get alert counts for maps', (done) => {
          let running = false;
          const interval = setInterval(() => {
            if (alertCreated && !running) {
              running = true;
              clearInterval(interval);
              const p = new Promise((resolve) => {
                a.getAlertCountsForMap(mapId, (data, error) => {
                  resolve(data);
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 2, 2, 4);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                });
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            }
          }, 1000);
        }).timeout(attemptTimeout);
      });

      describe('#getAlertCountsForMapById', () => {
        it('should get alert counts for maps by id', (done) => {
          let running = false;
          const interval = setInterval(() => {
            if (alertCreated && !running) {
              running = true;
              clearInterval(interval);
              const p = new Promise((resolve) => {
                a.getAlertCountsForMapById(mapId, (data, error) => {
                  resolve(data);
                  getAlertCount += 1;
                  runCommonAsserts(data, error);

                  if (stub) {
                    runAlertCountAsserts(data, 2, 2, 4);
                  } else {
                    assert.notEqual(0, data.response.alerts.length);
                  }

                  done();
                });
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            }
          }, 1000);
        }).timeout(attemptTimeout);
      });
    }

    describe('#updateAlert', () => {
      it('should update an alert', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (getAlertCount >= getFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.updateAlert(alertId, ualertObj, (data, error) => {
                try {
                  updAlertCount += 1;
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#assignAlert', () => {
      it('should assign an alert', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (getAlertCount >= getFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.assignAlert(alertId, 'username', (data, error) => {
                try {
                  updAlertCount += 1;
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#ignoreAlert', () => {
      it('should ignore an alert', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (getAlertCount >= getFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.ignoreAlert(alertId, 'ignore message', now + 3600000, (data, error) => {
                try {
                  updAlertCount += 1;
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });
    const updFinalAlertCount = 3;

    describe('#clearAlert', () => {
      it('should clear an alert', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (updAlertCount >= updFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.clearAlert(alertId, 'clear message', (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlert', () => {
      it('should delete an alert', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (updAlertCount >= updFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.deleteAlert(alertId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#deleteMapConnection', () => {
      it('should delete a map connection', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (updAlertCount >= updFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.deleteMapConnection(mapId, mapConnId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#deleteMapNode', () => {
      it('should delete a map node - node A', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (updAlertCount >= updFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.deleteMapNode(mapId, mapNodeAId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('should delete a map node - node B', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (updAlertCount >= updFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.deleteMapNode(mapId, mapNodeBId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#deleteMap', () => {
      it('should delete a map', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (updAlertCount >= updFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.deleteMap(mapId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#removeDeviceComponentFromGroup', () => {
      it('should remove a device component from a group', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (updAlertCount >= updFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.removeDeviceComponentFromGroup(deviceId, deviceCompId, componentGrpId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#deleteComponentGroup', () => {
      it('should delete a component group', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (updAlertCount >= updFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.deleteComponentGroup(componentGrpId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceComponent', () => {
      it('should delete a device component', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (updAlertCount >= updFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.deleteDeviceComponent(deviceId, deviceCompId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#removeDeviceFromGroup', () => {
      it('should remove a device from a group', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (updAlertCount >= updFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.removeDeviceFromGroup(deviceId, deviceGrpId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroup', () => {
      it('should delete a device group', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (updAlertCount >= updFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.deleteDeviceGroup(deviceGrpId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#deleteDevice', () => {
      it('should delete a device', (done) => {
        let running = false;
        const interval = setInterval(() => {
          if (updAlertCount >= updFinalAlertCount && !running) {
            running = true;
            clearInterval(interval);
            try {
              a.deleteDevice(deviceId, (data, error) => {
                try {
                  runCommonAsserts(data, error);
                  assert.equal('success', data.response);
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }
        }, 1000);
      }).timeout(attemptTimeout);
    });

    describe('#getObjectAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectAttachmentResources(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.deviceId);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('ObjectGroup', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'getObjectAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectsUpdateObjectAttachmentResourcesBodyParam = {
      deviceId: 7,
      ids: [
        3
      ],
      type: 'ObjectGroup'
    };
    describe('#updateObjectAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectAttachmentResources(555, reportAttachmentsObjectsUpdateObjectAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'updateObjectAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectAttachmentSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resultLimitSetting);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'getObjectAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectsUpdateObjectAttachmentSettingsBodyParam = {
      resultLimitSetting: {
        resultLimit: 2
      }
    };
    describe('#updateObjectAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectAttachmentSettings(555, reportAttachmentsObjectsUpdateObjectAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'updateObjectAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectAttachmentVisualizationSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'getObjectAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectsUpdateObjectAttachmentVisualizationSettingsBodyParam = {
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: true,
          formatTimestamp: true
        },
        dataPresentationSetting: {
          formatNumbers: true,
          precision: 6
        },
        tableSetting: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: false
        }
      }
    };
    describe('#updateObjectAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectAttachmentVisualizationSettings(555, reportAttachmentsObjectsUpdateObjectAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'updateObjectAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectsPartiallyUpdateObjectAttachmentVisualizationSettingsBodyParam = {
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: true,
          formatTimestamp: true
        },
        dataPresentationSetting: {
          formatNumbers: true,
          precision: 10
        },
        tableSetting: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: false
        }
      }
    };
    describe('#partiallyUpdateObjectAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateObjectAttachmentVisualizationSettings(555, reportAttachmentsObjectsPartiallyUpdateObjectAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'partiallyUpdateObjectAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectsCreateObjectAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createObjectAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createObjectAttachment(555, reportAttachmentsObjectsCreateObjectAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'createObjectAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceTypes(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(6, data.response.pageNumber);
                assert.equal(5, data.response.pageSize);
                assert.equal(6, data.response.totalElements);
                assert.equal(5, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'getDeviceTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceTypesCreateDeviceTypeBodyParam = {
      deviceType: {
        name: 'string',
        parentId: 5
      },
      objectTypes: [
        {
          extendedInfo: {},
          id: 4,
          indicatorTypes: [
            {
              allowMaximumValue: {},
              dataUnits: {},
              description: {},
              displayUnits: {},
              extendedInfo: {},
              format: {},
              isDefault: {},
              isEnabled: {},
              name: {},
              pluginId: {},
              pluginObjectTypeId: {},
              syntheticExpression: {},
              syntheticMaximumExpression: {}
            }
          ],
          isEditable: true,
          isEnabled: true,
          name: 'string',
          objectTypes: [
            {
              extendedInfo: {},
              id: {},
              indicatorTypes: {},
              isEditable: {},
              isEnabled: {},
              name: {},
              objectTypes: {},
              parentObjectTypeId: {},
              pluginId: {}
            }
          ],
          parentObjectTypeId: 8,
          pluginId: 3
        }
      ]
    };
    describe('#createDeviceType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceType(deviceTypesCreateDeviceTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.deviceType);
                assert.equal(true, Array.isArray(data.response.objectTypes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'createDeviceType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTypeForDeviceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceTypeForDeviceById(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'getDeviceTypeForDeviceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTypeById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceTypeById(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.deviceType);
                assert.equal(true, Array.isArray(data.response.objectTypes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'getDeviceTypeById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceTypeById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceTypeById(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'deleteDeviceTypeById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addMemberByIdToType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addMemberByIdToType(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'addMemberByIdToType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceTypeMemberById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceTypeMemberById(555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'deleteDeviceTypeMemberById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceComponentPartiallyUpdateObjectByIdBodyParam = {
      name: 'string',
      description: 'string',
      pollFrequency: 7
    };
    describe('#partiallyUpdateObjectById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateObjectById(555, 555, deviceComponentPartiallyUpdateObjectByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceComponent', 'partiallyUpdateObjectById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusMapAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatusMapAttachmentResources(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.mapId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsStatusMap', 'getStatusMapAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsStatusMapUpdateStatusMapAttachmentResourcesBodyParam = {
      mapId: 1
    };
    describe('#updateStatusMapAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateStatusMapAttachmentResources(555, reportAttachmentsStatusMapUpdateStatusMapAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsStatusMap', 'updateStatusMapAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsStatusMapCreateStatusMapAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createStatusMapAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createStatusMapAttachment(555, reportAttachmentsStatusMapCreateStatusMapAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsStatusMap', 'createStatusMapAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiKeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiKeys((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'getApiKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiKeysCreateApiKeyBodyParam = {
      applicationName: 'string',
      expirationDate: 6
    };
    describe('#createApiKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createApiKey(apiKeysCreateApiKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.apiKey);
                assert.equal('string', data.response.applicationName);
                assert.equal(9, data.response.creationDate);
                assert.equal(2, data.response.expirationDate);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'createApiKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiKeys - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApiKeys((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'deleteApiKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiKeysUpdateApiKeyBodyParam = {
      applicationName: 'string',
      expirationDate: 6
    };
    describe('#updateApiKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateApiKey('fakedata', apiKeysUpdateApiKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'updateApiKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApiKey('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'deleteApiKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiKeysForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiKeysForUser(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'getApiKeysForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiKeysCreateApiKeyForUserBodyParam = {
      applicationName: 'string',
      expirationDate: 3
    };
    describe('#createApiKeyForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createApiKeyForUser(555, apiKeysCreateApiKeyForUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.apiKey);
                assert.equal('string', data.response.applicationName);
                assert.equal(6, data.response.creationDate);
                assert.equal(10, data.response.expirationDate);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'createApiKeyForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiKeysForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApiKeysForUser(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'deleteApiKeysForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiKeysUpdateApiKeyForUserBodyParam = {
      applicationName: 'string',
      expirationDate: 6
    };
    describe('#updateApiKeyForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateApiKeyForUser(555, 'fakedata', apiKeysUpdateApiKeyForUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'updateApiKeyForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiKeyForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApiKeyForUser('fakedata', 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'deleteApiKeyForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesInDiscovery - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDevicesInDiscovery(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(4, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Discovery', 'getDevicesInDiscovery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const discoveryFilterDevicesInDiscoveryBodyParam = {
      allowAutomatic: false,
      allowManual: true,
      deviceFilter: {
        allowDelete: true,
        alternateName: 'string',
        deleted: true,
        description: 'string',
        disableConcurrentPolling: false,
        disablePolling: true,
        disableThresholding: false,
        ids: [
          10
        ],
        inDeviceGroupIds: [
          5
        ],
        ipAddress: 'string',
        metadata: {},
        name: 'string',
        new: true,
        notInDeviceGroupIds: [
          6
        ],
        numElements: 4,
        peerIds: [
          7
        ],
        pluginManagerId: 2,
        pollFrequency: 5,
        snmpVersion: 10,
        timezone: 'string',
        workhoursGroupId: 10
      },
      isWorking: false,
      queueIn: [
        'string'
      ],
      timeCompletedBetween: {
        key: 1,
        value: 7
      },
      timeQueuedBetween: {
        key: 1,
        value: 2
      },
      timeStartedBetween: {
        key: 5,
        value: 4
      },
      timeUpdatedBetween: {
        key: 10,
        value: 3
      },
      timesDiscoveredBetween: {
        key: 3,
        value: 3
      }
    };
    describe('#filterDevicesInDiscovery - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterDevicesInDiscovery(null, null, null, null, null, discoveryFilterDevicesInDiscoveryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(4, data.response.pageNumber);
                assert.equal(2, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(5, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Discovery', 'filterDevicesInDiscovery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceStatusById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceStatusById(555, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.allowAutomatic);
                assert.equal(true, data.response.allowManual);
                assert.equal('object', typeof data.response.device);
                assert.equal(8, data.response.deviceId);
                assert.equal(false, data.response.isWorking);
                assert.equal('BLANK', data.response.queue);
                assert.equal(1, data.response.timeCompleted);
                assert.equal(10, data.response.timeQueued);
                assert.equal(10, data.response.timeStarted);
                assert.equal(10, data.response.timeUpdated);
                assert.equal(10, data.response.timesDiscovered);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Discovery', 'getDeviceStatusById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const discoveryUpdateDevicePriorityBodyParam = {
      allowAutomatic: false,
      allowManual: true,
      isWorking: true,
      queue: 'high'
    };
    describe('#updateDevicePriority - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDevicePriority(555, discoveryUpdateDevicePriorityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Discovery', 'updateDevicePriority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentFilterSchema - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachmentFilterSchema((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.filters);
                assert.equal('object', typeof data.response.operations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachmentFilterSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachmentFilters(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.empty);
                assert.equal('string', data.response.expression);
                assert.equal(true, Array.isArray(data.response.filters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconUpdateFlowFalconAttachmentFiltersBodyParam = {
      empty: false,
      expression: 'string',
      filters: [
        {
          id: 'string',
          name: 'string',
          operation: 'string',
          values: [
            'string'
          ]
        }
      ]
    };
    describe('#updateFlowFalconAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFlowFalconAttachmentFilters(555, reportAttachmentsFlowFalconUpdateFlowFalconAttachmentFiltersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'updateFlowFalconAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachmentResources(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.groups);
                assert.equal(true, Array.isArray(data.response.interfaces));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconUpdateFlowFalconAttachmentResourcesBodyParam = {
      groups: {
        ids: [
          5
        ],
        type: 'ObjectGroup'
      },
      interfaces: [
        {
          direction: 9,
          interfaceNum: 8,
          sourceIp: 'string'
        }
      ]
    };
    describe('#updateFlowFalconAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFlowFalconAttachmentResources(555, reportAttachmentsFlowFalconUpdateFlowFalconAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'updateFlowFalconAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachmentSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.flowFalconResolutionSettings);
                assert.equal('object', typeof data.response.flowFalconSetting);
                assert.equal('object', typeof data.response.flowFalconTemplateSetting);
                assert.equal('object', typeof data.response.resultLimitSetting);
                assert.equal('object', typeof data.response.sourceFields);
                assert.equal('object', typeof data.response.unitsSetting);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconUpdateFlowFalconAttachmentSettingsBodyParam = {
      flowFalconResolutionSettings: {
        showAs: 'string',
        showDns: 'string',
        showDscp: 'string',
        showPort: 'string',
        showProtocol: 'string'
      },
      flowFalconSetting: {
        canZoomInCb: 'string',
        granularity: 3,
        graphOther: false,
        isRate: true,
        split: 2,
        subnetCategoryId: 9
      },
      flowFalconTemplateSetting: {
        isAggregated: true,
        metricId: 8,
        templateId: 1
      },
      resultLimitSetting: {
        resultLimit: 9
      },
      sourceFields: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      },
      unitsSetting: {
        percentage: true,
        preferredUnits: 'bytes'
      }
    };
    describe('#updateFlowFalconAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFlowFalconAttachmentSettings(555, reportAttachmentsFlowFalconUpdateFlowFalconAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'updateFlowFalconAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconPartiallyUpdateFlowFalconAttachmentSettingsBodyParam = {
      flowFalconResolutionSettings: {
        showAs: 'string',
        showDns: 'string',
        showDscp: 'string',
        showPort: 'string',
        showProtocol: 'string'
      },
      flowFalconSetting: {
        canZoomInCb: 'string',
        granularity: 3,
        graphOther: false,
        isRate: false,
        split: 10,
        subnetCategoryId: 3
      },
      flowFalconTemplateSetting: {
        isAggregated: true,
        metricId: 2,
        templateId: 10
      },
      resultLimitSetting: {
        resultLimit: 2
      },
      sourceFields: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      },
      unitsSetting: {
        percentage: false,
        preferredUnits: 'bytes'
      }
    };
    describe('#partiallyUpdateFlowFalconAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateFlowFalconAttachmentSettings(555, reportAttachmentsFlowFalconPartiallyUpdateFlowFalconAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'partiallyUpdateFlowFalconAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachmentTimeSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ranges);
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconUpdateFlowFalconAttachmentTimeSettingsBodyParam = {
      ranges: {
        end: 'string',
        start: 'string',
        type: 'CustomWeek'
      },
      timezone: 'string'
    };
    describe('#updateFlowFalconAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFlowFalconAttachmentTimeSettings(555, reportAttachmentsFlowFalconUpdateFlowFalconAttachmentTimeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'updateFlowFalconAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachmentVisualizationSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.pie);
                assert.equal('object', typeof data.response.stackedLine);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconUpdateFlowFalconAttachmentVisualizationSettingsBodyParam = {
      pie: {
        showAggregation: true,
        showAverage: false,
        showFrequency: false,
        showLast: true,
        showLegend: false,
        showPeak: false,
        showTimespan: true,
        showTitle: true,
        showValley: true
      },
      stackedLine: {
        drawGraphOutline: false,
        prettyXAxis: true,
        prettyYAxis: false,
        scaleToMinimumValue: true,
        scaled: false,
        sendGraphCSV: false,
        showAggregation: true,
        showAverage: false,
        showFrequency: true,
        showLast: true,
        showLegend: false,
        showPeak: true,
        showTimespan: true,
        showTitle: false,
        showValley: true
      },
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: false,
          formatTimestamp: true
        },
        dataPresentationSetting: {
          formatNumbers: false,
          precision: 1
        },
        tableSetting: {
          blockVis: false,
          hideHeaders: false,
          wrapCells: false
        }
      }
    };
    describe('#updateFlowFalconAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFlowFalconAttachmentVisualizationSettings(555, reportAttachmentsFlowFalconUpdateFlowFalconAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'updateFlowFalconAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconPartiallyUpdateFlowFalconAttachmentVisualizationSettingsBodyParam = {
      pie: {
        showAggregation: true,
        showAverage: false,
        showFrequency: true,
        showLast: true,
        showLegend: true,
        showPeak: true,
        showTimespan: true,
        showTitle: false,
        showValley: true
      },
      stackedLine: {
        drawGraphOutline: true,
        prettyXAxis: true,
        prettyYAxis: true,
        scaleToMinimumValue: false,
        scaled: false,
        sendGraphCSV: false,
        showAggregation: false,
        showAverage: true,
        showFrequency: false,
        showLast: true,
        showLegend: true,
        showPeak: false,
        showTimespan: false,
        showTitle: false,
        showValley: true
      },
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: false,
          formatTimestamp: false
        },
        dataPresentationSetting: {
          formatNumbers: false,
          precision: 6
        },
        tableSetting: {
          blockVis: false,
          hideHeaders: true,
          wrapCells: true
        }
      }
    };
    describe('#partiallyUpdateFlowFalconAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateFlowFalconAttachmentVisualizationSettings(555, reportAttachmentsFlowFalconPartiallyUpdateFlowFalconAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'partiallyUpdateFlowFalconAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconCreateFlowFalconAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createFlowFalconAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFlowFalconAttachment(555, reportAttachmentsFlowFalconCreateFlowFalconAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.timeSettings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'createFlowFalconAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopNAttachmentResources(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('DeviceGroup', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'getTopNAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNUpdateTopNAttachmentResourcesBodyParam = {
      ids: [
        8
      ],
      type: 'Device'
    };
    describe('#updateTopNAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopNAttachmentResources(555, reportAttachmentsTopNUpdateTopNAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'updateTopNAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopNAttachmentSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.aggregationSelectionSetting);
                assert.equal('object', typeof data.response.resultLimitSetting);
                assert.equal('object', typeof data.response.topNSetting);
                assert.equal('object', typeof data.response.unitsSetting);
                assert.equal('object', typeof data.response.workHoursSetting);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'getTopNAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNUpdateTopNAttachmentSettingsBodyParam = {
      aggregationSelectionSetting: {
        aggregationType: 'average',
        aggregationUnits: 'auto'
      },
      resultLimitSetting: {
        resultLimit: 8
      },
      topNSetting: {
        capacityThreshold: 'string',
        displayOnlyExceedingObjects: true,
        displayRValues: false,
        objectDescriptions: 'string',
        sortBy: 10,
        sortOrder: 'DESC',
        templateId: 1
      },
      unitsSetting: {
        percentage: true,
        preferredUnits: 'bits'
      },
      workHoursSetting: {
        customWorkhours: [
          {
            days: [
              'monday'
            ],
            startHour: 1,
            startMin: 6,
            endHour: 2,
            endMin: 10
          }
        ],
        type: 'system',
        workHoursGroupId: 8
      }
    };
    describe('#updateTopNAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopNAttachmentSettings(555, reportAttachmentsTopNUpdateTopNAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'updateTopNAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNPartiallyUpdateTopNAttachmentSettingsBodyParam = {
      aggregationSelectionSetting: {
        aggregationType: 'maximum',
        aggregationUnits: 'monthly'
      },
      resultLimitSetting: {
        resultLimit: 2
      },
      topNSetting: {
        capacityThreshold: 'string',
        displayOnlyExceedingObjects: true,
        displayRValues: false,
        objectDescriptions: 'string',
        sortBy: 10,
        sortOrder: 'ASC',
        templateId: 1
      },
      unitsSetting: {
        percentage: true,
        preferredUnits: 'bytes'
      },
      workHoursSetting: {
        customWorkhours: [
          {
            days: [
              'friday'
            ],
            startHour: 1,
            startMin: 7,
            endHour: 1,
            endMin: 8
          }
        ],
        type: 'custom',
        workHoursGroupId: 3
      }
    };
    describe('#partiallyUpdateTopNAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateTopNAttachmentSettings(555, reportAttachmentsTopNPartiallyUpdateTopNAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'partiallyUpdateTopNAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopNAttachmentTimeSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ranges);
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'getTopNAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNUpdateTopNAttachmentTimeSettingsBodyParam = {
      ranges: {
        end: 'string',
        start: 'string',
        type: 'CustomWeek'
      },
      timezone: 'string'
    };
    describe('#updateTopNAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopNAttachmentTimeSettings(555, reportAttachmentsTopNUpdateTopNAttachmentTimeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'updateTopNAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopNAttachmentVisualizationSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.pie);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'getTopNAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNUpdateTopNAttachmentVisualizationSettingsBodyParam = {
      pie: {
        showAggregation: false,
        showAverage: true,
        showFrequency: false,
        showLast: true,
        showLegend: false,
        showPeak: false,
        showTimespan: true,
        showTitle: true,
        showValley: true
      },
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: true,
          formatTimestamp: true
        },
        dataPresentationSetting: {
          formatNumbers: true,
          precision: 6
        },
        tableSetting: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: false
        }
      }
    };
    describe('#updateTopNAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopNAttachmentVisualizationSettings(555, reportAttachmentsTopNUpdateTopNAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'updateTopNAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNPartiallyUpdateTopNAttachmentVisualizationSettingsBodyParam = {
      pie: {
        showAggregation: false,
        showAverage: true,
        showFrequency: true,
        showLast: true,
        showLegend: true,
        showPeak: false,
        showTimespan: true,
        showTitle: true,
        showValley: true
      },
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: false,
          formatTimestamp: false
        },
        dataPresentationSetting: {
          formatNumbers: true,
          precision: 1
        },
        tableSetting: {
          blockVis: true,
          hideHeaders: true,
          wrapCells: false
        }
      }
    };
    describe('#partiallyUpdateTopNAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateTopNAttachmentVisualizationSettings(555, reportAttachmentsTopNPartiallyUpdateTopNAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'partiallyUpdateTopNAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNCreateTopNAttachmentBodyParam = {
      name: 'string',
      resource: {}
    };
    describe('#createTopNAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTopNAttachment(555, reportAttachmentsTopNCreateTopNAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.timeSettings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'createTopNAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPeersUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPeersUsingGET(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(8, data.response.pageNumber);
                assert.equal(7, data.response.pageSize);
                assert.equal(2, data.response.totalElements);
                assert.equal(9, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'getPeersUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getClusterSettings(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'getClusterSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrentPeerUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCurrentPeerUsingGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('PRIMARY', data.response.activeAppliance);
                assert.equal(10, data.response.capacity);
                assert.equal(true, data.response.disabled);
                assert.equal(10, data.response.droppedFlowsPerSecond);
                assert.equal(5, data.response.flowLimit);
                assert.equal(1, data.response.flowLoad);
                assert.equal(4, data.response.flowsPerSecond);
                assert.equal(4, data.response.interfaceLimit);
                assert.equal('string', data.response.ip);
                assert.equal(true, data.response.master);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.netflowDeviceCount);
                assert.equal(9, data.response.netflowInterfaceCount);
                assert.equal('string', data.response.primaryIp);
                assert.equal(9, data.response.processedFlowsPerSecond);
                assert.equal('string', data.response.secondaryIp);
                assert.equal(6, data.response.serverId);
                assert.equal(4, data.response.serverLoad);
                assert.equal(1, data.response.telephonyActiveCallsLimit);
                assert.equal(4, data.response.telephonyPhones);
                assert.equal(5, data.response.telephonyPhonesLimit);
                assert.equal(10, data.response.totalCapacity);
                assert.equal(5, data.response.totalLoad);
                assert.equal('string', data.response.virtualIp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'getCurrentPeerUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncorporateModeUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIncorporateModeUsingGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.endTime);
                assert.equal(9, data.response.startTime);
                assert.equal('ACTIVE', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'getIncorporateModeUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const peersEditIncorporateModeUsingPATCHBodyParam = {
      status: 'ACTIVE'
    };
    describe('#editIncorporateModeUsingPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIncorporateModeUsingPATCH(peersEditIncorporateModeUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'editIncorporateModeUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPeerUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPeerUsingGET(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('PRIMARY', data.response.activeAppliance);
                assert.equal(9, data.response.capacity);
                assert.equal(false, data.response.disabled);
                assert.equal(1, data.response.droppedFlowsPerSecond);
                assert.equal(9, data.response.flowLimit);
                assert.equal(8, data.response.flowLoad);
                assert.equal(1, data.response.flowsPerSecond);
                assert.equal(1, data.response.interfaceLimit);
                assert.equal('string', data.response.ip);
                assert.equal(false, data.response.master);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.name);
                assert.equal(9, data.response.netflowDeviceCount);
                assert.equal(1, data.response.netflowInterfaceCount);
                assert.equal('string', data.response.primaryIp);
                assert.equal(3, data.response.processedFlowsPerSecond);
                assert.equal('string', data.response.secondaryIp);
                assert.equal(2, data.response.serverId);
                assert.equal(2, data.response.serverLoad);
                assert.equal(8, data.response.telephonyActiveCallsLimit);
                assert.equal(3, data.response.telephonyPhones);
                assert.equal(7, data.response.telephonyPhonesLimit);
                assert.equal(2, data.response.totalCapacity);
                assert.equal(1, data.response.totalLoad);
                assert.equal('string', data.response.virtualIp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'getPeerUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSettings(555, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'getSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReportAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReportAttachment(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachments', 'getReportAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsUpdateReportAttachmentByIdBodyParam = {
      name: 'string'
    };
    describe('#updateReportAttachmentById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateReportAttachmentById(555, reportAttachmentsUpdateReportAttachmentByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachments', 'updateReportAttachmentById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReportAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteReportAttachment(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachments', 'deleteReportAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllReportAttachmentEndpoints - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllReportAttachmentEndpoints('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachments', 'getAllReportAttachmentEndpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllReportAttachments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllReportAttachments(555, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(10, data.response.pageNumber);
                assert.equal(2, data.response.pageSize);
                assert.equal(3, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachments', 'getAllReportAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupsRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupsRules(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(6, data.response.pageNumber);
                assert.equal(6, data.response.pageSize);
                assert.equal(2, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'getObjectGroupsRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectGroupRulesCreateObjectGroupRuleBodyParam = {
      attributeId: 4,
      descriptionExpression: 'string',
      deviceGroup: 1,
      deviceType: 6,
      groupId: 7,
      id: 7,
      metadataValueExpression: 'string',
      nameExpression: 'string',
      namespaceId: 2,
      objectTypeId: 10,
      pluginId: 7,
      subtypeId: 7
    };
    describe('#createObjectGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createObjectGroupRule(objectGroupRulesCreateObjectGroupRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.attributeId);
                assert.equal('string', data.response.descriptionExpression);
                assert.equal(2, data.response.deviceGroup);
                assert.equal(9, data.response.deviceType);
                assert.equal(3, data.response.groupId);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.metadataValueExpression);
                assert.equal('string', data.response.nameExpression);
                assert.equal(4, data.response.namespaceId);
                assert.equal(6, data.response.objectTypeId);
                assert.equal(4, data.response.pluginId);
                assert.equal(10, data.response.subtypeId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'createObjectGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupRule(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.attributeId);
                assert.equal('string', data.response.descriptionExpression);
                assert.equal(8, data.response.deviceGroup);
                assert.equal(4, data.response.deviceType);
                assert.equal(4, data.response.groupId);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.metadataValueExpression);
                assert.equal('string', data.response.nameExpression);
                assert.equal(10, data.response.namespaceId);
                assert.equal(5, data.response.objectTypeId);
                assert.equal(3, data.response.pluginId);
                assert.equal(10, data.response.subtypeId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'getObjectGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectGroupRulesUpdateObjectGroupRuleBodyParam = {
      attributeId: 10,
      descriptionExpression: 'string',
      deviceGroup: 6,
      deviceType: 7,
      groupId: 5,
      id: 3,
      metadataValueExpression: 'string',
      nameExpression: 'string',
      namespaceId: 9,
      objectTypeId: 9,
      pluginId: 6,
      subtypeId: 9
    };
    describe('#updateObjectGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectGroupRule(555, objectGroupRulesUpdateObjectGroupRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'updateObjectGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteObjectGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteObjectGroupRule(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'deleteObjectGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupRules(555, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(3, data.response.pageNumber);
                assert.equal(1, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(1, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'getObjectGroupRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyObjectGroupRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applyObjectGroupRules(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal(100, data.response.statusCode);
                assert.equal(3, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'applyObjectGroupRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrentUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCurrentUser((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('LDAP', data.response.authentication);
                assert.equal(8, data.response.customTimeout);
                assert.equal('string', data.response.dateFormat);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.firstName);
                assert.equal(true, data.response.forcePasswordChange);
                assert.equal(5, data.response.id);
                assert.equal(true, data.response.isActive);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.password);
                assert.equal(true, data.response.passwordNeverExpires);
                assert.equal(true, Array.isArray(data.response.roleIds));
                assert.equal('string', data.response.timezone);
                assert.equal(true, data.response.timezoneStartupCheck);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getCurrentUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupAttachmentResources(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.deviceId);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('ObjectGroup', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'getObjectGroupAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectGroupsUpdateObjectGroupAttachmentResourcesBodyParam = {
      deviceId: 1,
      ids: [
        5
      ],
      type: 'ObjectGroup'
    };
    describe('#updateObjectGroupAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectGroupAttachmentResources(555, reportAttachmentsObjectGroupsUpdateObjectGroupAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'updateObjectGroupAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupAttachmentVisualizationSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'getObjectGroupAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectGroupsUpdateObjectGroupAttachmentVisualizationSettingsBodyParam = {
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: true,
          formatTimestamp: false
        },
        dataPresentationSetting: {
          formatNumbers: false,
          precision: 6
        },
        tableSetting: {
          blockVis: false,
          hideHeaders: true,
          wrapCells: true
        }
      }
    };
    describe('#updateObjectGroupAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectGroupAttachmentVisualizationSettings(555, reportAttachmentsObjectGroupsUpdateObjectGroupAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'updateObjectGroupAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectGroupsPartiallyUpdateObjectGroupAttachmentVisualizationSettingsBodyParam = {
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: false,
          formatTimestamp: false
        },
        dataPresentationSetting: {
          formatNumbers: false,
          precision: 7
        },
        tableSetting: {
          blockVis: true,
          hideHeaders: true,
          wrapCells: true
        }
      }
    };
    describe('#partiallyUpdateObjectGroupAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateObjectGroupAttachmentVisualizationSettings(555, reportAttachmentsObjectGroupsPartiallyUpdateObjectGroupAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'partiallyUpdateObjectGroupAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectGroupsCreateObjectGroupAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createObjectGroupAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createObjectGroupAttachment(555, reportAttachmentsObjectGroupsCreateObjectGroupAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'createObjectGroupAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mapPartiallyUpdateStatusMapByIdBodyParam = {
      imageId: 6,
      name: 'string'
    };
    describe('#partiallyUpdateStatusMapById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateStatusMapById(555, mapPartiallyUpdateStatusMapByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Map', 'partiallyUpdateStatusMapById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachmentFilterSchema - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceAttachmentFilterSchema((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.filters);
                assert.equal('object', typeof data.response.operations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'getDeviceAttachmentFilterSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceAttachmentFilters(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.empty);
                assert.equal('string', data.response.expression);
                assert.equal(true, Array.isArray(data.response.filters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'getDeviceAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDevicesUpdateDeviceAttachmentFiltersBodyParam = {
      empty: true,
      expression: 'string',
      filters: [
        {
          id: 'string',
          name: 'string',
          operation: 'string',
          values: [
            'string'
          ]
        }
      ]
    };
    describe('#updateDeviceAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceAttachmentFilters(555, reportAttachmentsDevicesUpdateDeviceAttachmentFiltersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'updateDeviceAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceAttachment(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('Everything', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'getDeviceAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDevicesUpdateDeviceAttachmentBodyParam = {
      ids: [
        6
      ],
      type: 'Device'
    };
    describe('#updateDeviceAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceAttachment(555, reportAttachmentsDevicesUpdateDeviceAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'updateDeviceAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceAttachmentSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resultLimitSetting);
                assert.equal('object', typeof data.response.sourceFieldsSetting);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'getDeviceAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDevicesUpdateDeviceAttachmentSettingsBodyParam = {
      resultLimitSetting: {
        resultLimit: 3
      },
      sourceFieldsSetting: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      }
    };
    describe('#updateDeviceAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceAttachmentSettings(555, reportAttachmentsDevicesUpdateDeviceAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'updateDeviceAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachmentVisualization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceAttachmentVisualization(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'getDeviceAttachmentVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDevicesUpdateDeviceAttachmentVisualizationBodyParam = {
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: false,
          formatTimestamp: false
        },
        dataPresentationSetting: {
          formatNumbers: false,
          precision: 6
        },
        tableSetting: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: false
        }
      }
    };
    describe('#updateDeviceAttachmentVisualization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceAttachmentVisualization(555, reportAttachmentsDevicesUpdateDeviceAttachmentVisualizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'updateDeviceAttachmentVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDevicesCreateDeviceAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createDeviceAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceAttachment(555, reportAttachmentsDevicesCreateDeviceAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'createDeviceAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupsAttachment(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('DeviceGroup', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDeviceGroups', 'getDeviceGroupsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDeviceGroupsUpdateDeviceGroupsAttachmentBodyParam = {
      ids: [
        5
      ],
      type: 'DeviceGroup'
    };
    describe('#updateDeviceGroupsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupsAttachment(555, reportAttachmentsDeviceGroupsUpdateDeviceGroupsAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDeviceGroups', 'updateDeviceGroupsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupsAttachmentVisualization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupsAttachmentVisualization(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDeviceGroups', 'getDeviceGroupsAttachmentVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDeviceGroupsUpdateDeviceGroupsAttachmentVisualizationBodyParam = {
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: false,
          formatTimestamp: true
        },
        dataPresentationSetting: {
          formatNumbers: false,
          precision: 10
        },
        tableSetting: {
          blockVis: false,
          hideHeaders: true,
          wrapCells: false
        }
      }
    };
    describe('#updateDeviceGroupsAttachmentVisualization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupsAttachmentVisualization(555, reportAttachmentsDeviceGroupsUpdateDeviceGroupsAttachmentVisualizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDeviceGroups', 'updateDeviceGroupsAttachmentVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDeviceGroupsCreateDeviceGroupsAttachmentBodyParam = {
      name: 'string',
      resource: {}
    };
    describe('#createDeviceGroupsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceGroupsAttachment(555, reportAttachmentsDeviceGroupsCreateDeviceGroupsAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDeviceGroups', 'createDeviceGroupsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetadataAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMetadataAttachmentResources(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.deviceId);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('ObjectGroup', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'getMetadataAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsMetadataUpdateMetadataAttachmentResourcesBodyParam = {
      deviceId: 10,
      ids: [
        4
      ],
      type: 'Device'
    };
    describe('#updateMetadataAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMetadataAttachmentResources(555, reportAttachmentsMetadataUpdateMetadataAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'updateMetadataAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetadataAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMetadataAttachmentVisualizationSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'getMetadataAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsMetadataUpdateMetadataAttachmentVisualizationSettingsBodyParam = {
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: true,
          formatTimestamp: true
        },
        dataPresentationSetting: {
          formatNumbers: true,
          precision: 9
        },
        tableSetting: {
          blockVis: false,
          hideHeaders: false,
          wrapCells: true
        }
      }
    };
    describe('#updateMetadataAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMetadataAttachmentVisualizationSettings(555, reportAttachmentsMetadataUpdateMetadataAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'updateMetadataAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsMetadataPartiallyUpdateMetadataAttachmentVisualizationSettingsBodyParam = {
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: true,
          formatTimestamp: true
        },
        dataPresentationSetting: {
          formatNumbers: false,
          precision: 6
        },
        tableSetting: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: false
        }
      }
    };
    describe('#partiallyUpdateMetadataAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateMetadataAttachmentVisualizationSettings(555, reportAttachmentsMetadataPartiallyUpdateMetadataAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'partiallyUpdateMetadataAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsMetadataCreateMetadataAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createMetadataAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMetadataAttachment(555, reportAttachmentsMetadataCreateMetadataAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'createMetadataAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerformanceMetricsAttachmentResources(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.group);
                assert.equal(true, Array.isArray(data.response.indicators));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'getPerformanceMetricsAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentResourcesBodyParam = {
      group: {
        id: 7,
        indicatorTypes: {
          ids: [
            5
          ],
          objectTypeId: 8,
          pluginId: 1
        },
        type: 'DeviceGroup'
      },
      indicators: [
        {
          deviceId: 6,
          ids: [
            6
          ],
          objectId: 3,
          pluginId: 2
        }
      ]
    };
    describe('#updatePerformanceMetricsAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentResources(555, reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'updatePerformanceMetricsAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerformanceMetricsAttachmentSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.rawDataSetting);
                assert.equal('object', typeof data.response.sourceFields);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'getPerformanceMetricsAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentSettingsBodyParam = {
      rawDataSetting: {
        dataAggregationSetting: {
          aggregationType: 'autoStackedBar',
          aggregationUnits: 'day',
          aggregationValue: 2,
          useAggregation: true
        },
        rawDataSetting: {
          fitTimeSpanTo: 'months',
          percentiles: 1,
          projectionTime: 10,
          reduceData: false,
          standardDeviation: 2,
          timeOverTimeOnly: false,
          timeOverTimeType: 'maximum',
          timeOverTimeUnits: 'hour',
          timeOverTimeValue: 8,
          trend: 'standard',
          trendType: 'exponential',
          useBaseline: true,
          usePercentiles: 'string',
          useTimeOverTime: true
        },
        unitsSetting: {
          percentage: false,
          preferredUnits: 'bytes'
        },
        workHoursSetting: {
          customWorkhours: [
            {
              days: {},
              startHour: {},
              startMin: {},
              endHour: {},
              endMin: {}
            }
          ],
          type: 'none',
          workHoursGroupId: 5
        }
      },
      sourceFields: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      }
    };
    describe('#updatePerformanceMetricsAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentSettings(555, reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'updatePerformanceMetricsAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsPartiallyUpdatePerformanceMetricsAttachmentSettingsBodyParam = {
      rawDataSetting: {
        dataAggregationSetting: {
          aggregationType: 'minimum',
          aggregationUnits: 'week',
          aggregationValue: 7,
          useAggregation: false
        },
        rawDataSetting: {
          fitTimeSpanTo: 'day',
          percentiles: 9,
          projectionTime: 6,
          reduceData: false,
          standardDeviation: 6,
          timeOverTimeOnly: false,
          timeOverTimeType: 'total',
          timeOverTimeUnits: 'second',
          timeOverTimeValue: 8,
          trend: 'projected',
          trendType: 'power',
          useBaseline: false,
          usePercentiles: 'string',
          useTimeOverTime: false
        },
        unitsSetting: {
          percentage: false,
          preferredUnits: 'bits'
        },
        workHoursSetting: {
          customWorkhours: [
            {
              days: {},
              startHour: {},
              startMin: {},
              endHour: {},
              endMin: {}
            }
          ],
          type: 'device',
          workHoursGroupId: 4
        }
      },
      sourceFields: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      }
    };
    describe('#partiallyUpdatePerformanceMetricsAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdatePerformanceMetricsAttachmentSettings(555, reportAttachmentsPerformanceMetricsPartiallyUpdatePerformanceMetricsAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'partiallyUpdatePerformanceMetricsAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerformanceMetricsAttachmentTimeSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ranges);
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'getPerformanceMetricsAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentTimeSettingsBodyParam = {
      ranges: {
        end: 'string',
        start: 'string',
        type: 'RelativePeriod'
      },
      timezone: 'string'
    };
    describe('#updatePerformanceMetricsAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentTimeSettings(555, reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentTimeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'updatePerformanceMetricsAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerformanceMetricsAttachmentVisualizationSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.bar);
                assert.equal('object', typeof data.response.line);
                assert.equal('object', typeof data.response.pie);
                assert.equal('object', typeof data.response.radial);
                assert.equal('object', typeof data.response.stackedBar);
                assert.equal('object', typeof data.response.stackedLine);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'getPerformanceMetricsAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentVisualizationSettingsBodyParam = {
      bar: {
        drawGraphOutline: false,
        prettyXAxis: false,
        prettyYAxis: true,
        scaleToMinimumValue: true,
        scaled: false,
        showAggregation: true,
        showAverage: false,
        showFrequency: false,
        showLast: false,
        showLegend: false,
        showPeak: true,
        showTimespan: true,
        showTitle: true,
        showValley: false
      },
      line: {
        drawGraphOutline: false,
        dualYAxes: true,
        logScale: false,
        prettyXAxis: true,
        prettyYAxis: false,
        scaleToMinimumValue: true,
        scaled: true,
        showAggregation: true,
        showAverage: false,
        showFrequency: false,
        showLast: true,
        showLegend: false,
        showPeak: false,
        showTimespan: false,
        showTitle: true,
        showValley: true
      },
      pie: {
        showAggregation: false,
        showAverage: true,
        showFrequency: true,
        showLast: false,
        showLegend: true,
        showPeak: true,
        showTimespan: true,
        showTitle: true,
        showValley: false
      },
      radial: {
        drawGraphOutline: false,
        scaled: false,
        showAggregation: true,
        showAverage: true,
        showFrequency: true,
        showLast: true,
        showLegend: false,
        showPeak: false,
        showTimespan: true,
        showTitle: true,
        showValley: false
      },
      stackedBar: {
        drawGraphOutline: false,
        prettyXAxis: true,
        prettyYAxis: true,
        scaled: true,
        showAggregation: false,
        showAverage: false,
        showFrequency: true,
        showLast: false,
        showLegend: true,
        showPeak: false,
        showTimespan: false,
        showTitle: false,
        showValley: false
      },
      stackedLine: {
        drawGraphOutline: true,
        prettyXAxis: true,
        prettyYAxis: false,
        scaleToMinimumValue: true,
        scaled: false,
        sendGraphCSV: true,
        showAggregation: true,
        showAverage: false,
        showFrequency: true,
        showLast: false,
        showLegend: false,
        showPeak: false,
        showTimespan: false,
        showTitle: true,
        showValley: true
      },
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: true,
          formatTimestamp: false
        },
        dataPresentationSetting: {
          formatNumbers: true,
          precision: 4
        },
        tableSetting: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: true
        }
      }
    };
    describe('#updatePerformanceMetricsAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentVisualizationSettings(555, reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'updatePerformanceMetricsAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsPartiallyUpdatePerformanceMetricsAttachmentVisualizationSettingsBodyParam = {
      bar: {
        drawGraphOutline: false,
        prettyXAxis: false,
        prettyYAxis: false,
        scaleToMinimumValue: true,
        scaled: false,
        showAggregation: true,
        showAverage: true,
        showFrequency: false,
        showLast: false,
        showLegend: true,
        showPeak: true,
        showTimespan: false,
        showTitle: false,
        showValley: true
      },
      line: {
        drawGraphOutline: false,
        dualYAxes: false,
        logScale: false,
        prettyXAxis: true,
        prettyYAxis: false,
        scaleToMinimumValue: true,
        scaled: true,
        showAggregation: false,
        showAverage: false,
        showFrequency: false,
        showLast: false,
        showLegend: true,
        showPeak: false,
        showTimespan: true,
        showTitle: false,
        showValley: false
      },
      pie: {
        showAggregation: false,
        showAverage: false,
        showFrequency: true,
        showLast: true,
        showLegend: true,
        showPeak: false,
        showTimespan: true,
        showTitle: false,
        showValley: false
      },
      radial: {
        drawGraphOutline: true,
        scaled: false,
        showAggregation: false,
        showAverage: true,
        showFrequency: true,
        showLast: false,
        showLegend: false,
        showPeak: true,
        showTimespan: false,
        showTitle: true,
        showValley: true
      },
      stackedBar: {
        drawGraphOutline: true,
        prettyXAxis: true,
        prettyYAxis: false,
        scaled: false,
        showAggregation: true,
        showAverage: false,
        showFrequency: false,
        showLast: false,
        showLegend: false,
        showPeak: false,
        showTimespan: false,
        showTitle: true,
        showValley: false
      },
      stackedLine: {
        drawGraphOutline: false,
        prettyXAxis: false,
        prettyYAxis: true,
        scaleToMinimumValue: true,
        scaled: true,
        sendGraphCSV: false,
        showAggregation: false,
        showAverage: true,
        showFrequency: false,
        showLast: true,
        showLegend: true,
        showPeak: false,
        showTimespan: true,
        showTitle: true,
        showValley: true
      },
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: false,
          formatTimestamp: true
        },
        dataPresentationSetting: {
          formatNumbers: false,
          precision: 8
        },
        tableSetting: {
          blockVis: false,
          hideHeaders: true,
          wrapCells: false
        }
      }
    };
    describe('#partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings(555, reportAttachmentsPerformanceMetricsPartiallyUpdatePerformanceMetricsAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsCreatePerformanceMetricsAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createPerformanceMetricsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPerformanceMetricsAttachment(555, reportAttachmentsPerformanceMetricsCreatePerformanceMetricsAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.timeSettings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'createPerformanceMetricsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupMetricsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupMetricsAttachment(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal(true, Array.isArray(data.response.indicatorTypes));
                assert.equal('DeviceGroupChildren', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'getGroupMetricsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentBodyParam = {
      indicatorTypes: [
        {}
      ],
      type: 'ObjectGroup'
    };
    describe('#updateGroupMetricsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroupMetricsAttachment(555, reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'updateGroupMetricsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupMetricsAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupMetricsAttachmentSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.childrenReport);
                assert.equal('bits', data.response.preferredUnits);
                assert.equal('object', typeof data.response.sourceFields);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'getGroupMetricsAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentSettingsBodyParam = {
      childrenReport: true,
      preferredUnits: 'bytes',
      sourceFields: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      }
    };
    describe('#updateGroupMetricsAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroupMetricsAttachmentSettings(555, reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'updateGroupMetricsAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupMetricsAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupMetricsAttachmentTimeSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ranges);
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'getGroupMetricsAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentTimeSettingsBodyParam = {
      ranges: {
        end: 'string',
        start: 'string',
        type: 'RelativePeriod'
      },
      timezone: 'string'
    };
    describe('#updateGroupMetricsAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroupMetricsAttachmentTimeSettings(555, reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentTimeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'updateGroupMetricsAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupMetricsAttachmentVisualization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupMetricsAttachmentVisualization(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'getGroupMetricsAttachmentVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentVisualizationBodyParam = {
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: false,
          formatTimestamp: false
        },
        dataPresentationSetting: {
          formatNumbers: false,
          precision: 9
        },
        tableSetting: {
          blockVis: false,
          hideHeaders: true,
          wrapCells: true
        }
      }
    };
    describe('#updateGroupMetricsAttachmentVisualization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroupMetricsAttachmentVisualization(555, reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentVisualizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'updateGroupMetricsAttachmentVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsGroupMetricsCreateGroupMetricsAttachmentBodyParam = {
      name: 'string',
      resource: {}
    };
    describe('#createGroupMetricsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createGroupMetricsAttachment(555, reportAttachmentsGroupMetricsCreateGroupMetricsAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.timeSettings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'createGroupMetricsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveSessions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getActiveSessions(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'getActiveSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#keepAlive - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.keepAlive((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'keepAlive', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationSignInBodyParam = {
      name: 'string',
      password: 'string'
    };
    describe('#signIn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.signIn(null, authenticationSignInBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.token);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'signIn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#signOut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.signOut((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'signOut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#signOutOthers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.signOutOthers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'signOutOthers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#signOutUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.signOutUser(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'signOutUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupPartiallyUpdateDeviceGroupByIdBodyParam = {
      name: 'string',
      parentId: 10
    };
    describe('#partiallyUpdateDeviceGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateDeviceGroupById(555, deviceGroupPartiallyUpdateDeviceGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'partiallyUpdateDeviceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTagsById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceTagsById(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.parentId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'getDeviceTagsById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentFilterSchema - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentFilterSchema((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.filters);
                assert.equal('object', typeof data.response.operations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentFilterSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentAggregation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentAggregation(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('None', data.response.aggregation);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentAggregation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsUpdateAlertAttachmentAggregationBodyParam = {
      aggregation: 'None'
    };
    describe('#updateAlertAttachmentAggregation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertAttachmentAggregation(555, reportAttachmentsAlertsUpdateAlertAttachmentAggregationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'updateAlertAttachmentAggregation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentFilters(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.empty);
                assert.equal('string', data.response.expression);
                assert.equal(true, Array.isArray(data.response.filters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsUpdateAlertAttachmentFiltersBodyParam = {
      empty: true,
      expression: 'string',
      filters: [
        {
          id: 'string',
          name: 'string',
          operation: 'string',
          values: [
            'string'
          ]
        }
      ]
    };
    describe('#updateAlertAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertAttachmentFilters(555, reportAttachmentsAlertsUpdateAlertAttachmentFiltersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'updateAlertAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentResource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentResource(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('Everything', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsUpdateAlertAttachmentResourceBodyParam = {
      ids: [
        4
      ],
      type: 'DeviceGroup'
    };
    describe('#updateAlertAttachmentResource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertAttachmentResource(555, reportAttachmentsAlertsUpdateAlertAttachmentResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'updateAlertAttachmentResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.alertSetting);
                assert.equal('object', typeof data.response.resultLimitSetting);
                assert.equal('object', typeof data.response.sourceFieldsSetting);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsUpdateAlertAttachmentSettingsBodyParam = {
      alertSetting: {
        defaultView: 'groups',
        eventSummary: true,
        inlcudeMaintenanceWindowAlerts: false
      },
      resultLimitSetting: {
        resultLimit: 6
      },
      sourceFieldsSetting: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      }
    };
    describe('#updateAlertAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertAttachmentSettings(555, reportAttachmentsAlertsUpdateAlertAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'updateAlertAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentTimeSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ranges);
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsUpdateAlertAttachmentTimeSettingsBodyParam = {
      ranges: {
        end: 'string',
        start: 'string',
        type: 'CustomWeek'
      },
      timezone: 'string'
    };
    describe('#updateAlertAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertAttachmentTimeSettings(555, reportAttachmentsAlertsUpdateAlertAttachmentTimeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'updateAlertAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentVisualizationSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.bar);
                assert.equal('object', typeof data.response.pie);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsUpdateAlertAttachmentVisualizationSettingsBodyParam = {
      bar: {
        drawGraphOutline: true,
        prettyXAxis: true,
        prettyYAxis: false,
        scaleToMinimumValue: true,
        scaled: false,
        showAggregation: true,
        showAverage: true,
        showFrequency: true,
        showLast: true,
        showLegend: false,
        showPeak: false,
        showTimespan: true,
        showTitle: true,
        showValley: true
      },
      pie: {
        showAggregation: true,
        showAverage: false,
        showFrequency: true,
        showLast: true,
        showLegend: false,
        showPeak: false,
        showTimespan: true,
        showTitle: false,
        showValley: true
      },
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: false,
          formatTimestamp: true
        },
        dataPresentationSetting: {
          formatNumbers: true,
          precision: 7
        },
        tableSetting: {
          blockVis: false,
          hideHeaders: false,
          wrapCells: false
        }
      }
    };
    describe('#updateAlertAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertAttachmentVisualizationSettings(555, reportAttachmentsAlertsUpdateAlertAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'updateAlertAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsPartiallyUpdateAlertAttachmentVisualizationSettingsBodyParam = {
      bar: {
        drawGraphOutline: true,
        prettyXAxis: false,
        prettyYAxis: true,
        scaleToMinimumValue: true,
        scaled: false,
        showAggregation: true,
        showAverage: true,
        showFrequency: true,
        showLast: true,
        showLegend: false,
        showPeak: true,
        showTimespan: true,
        showTitle: true,
        showValley: false
      },
      pie: {
        showAggregation: true,
        showAverage: true,
        showFrequency: false,
        showLast: true,
        showLegend: false,
        showPeak: false,
        showTimespan: false,
        showTitle: false,
        showValley: true
      },
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: true,
          formatTimestamp: false
        },
        dataPresentationSetting: {
          formatNumbers: false,
          precision: 7
        },
        tableSetting: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: true
        }
      }
    };
    describe('#partiallyUpdateAlertAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateAlertAttachmentVisualizationSettings(555, reportAttachmentsAlertsPartiallyUpdateAlertAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'partiallyUpdateAlertAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsCreateAlertAttachmentBodyParam = {
      name: 'string',
      resource: {}
    };
    describe('#createAlertAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAlertAttachment(555, reportAttachmentsAlertsCreateAlertAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.timeSettings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'createAlertAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPlugins - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPlugins(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(1, data.response.totalElements);
                assert.equal(9, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getAllPlugins', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicePluginInfoSchema - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDevicePluginInfoSchema((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getDevicePluginInfoSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIndicatorExtendedInfoSchema - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIndicatorExtendedInfoSchema(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getIndicatorExtendedInfoSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPluginIndicatorTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPluginIndicatorTypes(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(5, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(1, data.response.totalElements);
                assert.equal(1, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getAllPluginIndicatorTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsCreatePluginIndicatorTypeBodyParam = {
      allowMaximumValue: true,
      dataUnits: 'string',
      description: 'string',
      displayUnits: 'string',
      extendedInfo: {},
      format: 'COUNTER64',
      id: 5,
      isDefault: true,
      isEnabled: false,
      name: 'string',
      pluginObjectTypeId: 3,
      syntheticExpression: 'string',
      syntheticMaximumExpression: 'string'
    };
    describe('#createPluginIndicatorType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPluginIndicatorType(pluginsCreatePluginIndicatorTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.allowMaximumValue);
                assert.equal('string', data.response.dataUnits);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.displayUnits);
                assert.equal('object', typeof data.response.extendedInfo);
                assert.equal('GAUGE', data.response.format);
                assert.equal(7, data.response.id);
                assert.equal(true, data.response.isDefault);
                assert.equal(false, data.response.isEnabled);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.pluginId);
                assert.equal(9, data.response.pluginObjectTypeId);
                assert.equal('string', data.response.syntheticExpression);
                assert.equal('string', data.response.syntheticMaximumExpression);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'createPluginIndicatorType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsFilterPluginIndicatorTypesBodyParam = {
      allowMaximumValue: false,
      dataUnits: 'string',
      description: 'string',
      displayUnits: 'string',
      format: 'GAUGE',
      id: 6,
      ids: [
        7
      ],
      isDefault: true,
      isEnabled: true,
      name: 'string',
      pluginId: 7,
      pluginObjectTypeId: 4,
      syntheticExpression: 'string',
      syntheticMaximumExpression: 'string'
    };
    describe('#filterPluginIndicatorTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterPluginIndicatorTypes(null, null, null, pluginsFilterPluginIndicatorTypesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(10, data.response.pageNumber);
                assert.equal(5, data.response.pageSize);
                assert.equal(1, data.response.totalElements);
                assert.equal(5, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'filterPluginIndicatorTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchemaForAllPluginIndicatorTypes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSchemaForAllPluginIndicatorTypes(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getSchemaForAllPluginIndicatorTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsUpdatePluginIndicatorTypeBodyParam = {
      allowMaximumValue: true,
      dataUnits: 'string',
      description: 'string',
      displayUnits: 'string',
      extendedInfo: {},
      format: 'GAUGE',
      id: 10,
      isDefault: false,
      isEnabled: false,
      name: 'string',
      pluginObjectTypeId: 7,
      syntheticExpression: 'string',
      syntheticMaximumExpression: 'string'
    };
    describe('#updatePluginIndicatorType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePluginIndicatorType(555, pluginsUpdatePluginIndicatorTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'updatePluginIndicatorType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectExtendedInfoSchema - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getObjectExtendedInfoSchema(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getObjectExtendedInfoSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPluginObjectTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPluginObjectTypes(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(6, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(7, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getAllPluginObjectTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsCreatePluginObjectTypeBodyParam = {
      extendedInfo: {},
      id: 3,
      isEditable: true,
      isEnabled: true,
      name: 'string',
      parentObjectTypeId: 7,
      pluginId: 3
    };
    describe('#createPluginObjectType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPluginObjectType(pluginsCreatePluginObjectTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.extendedInfo);
                assert.equal(3, data.response.id);
                assert.equal(true, data.response.isEditable);
                assert.equal(true, data.response.isEnabled);
                assert.equal('string', data.response.name);
                assert.equal(6, data.response.parentObjectTypeId);
                assert.equal(2, data.response.pluginId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'createPluginObjectType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsFilterPluginObjectTypesBodyParam = {
      id: 4,
      ids: [
        3
      ],
      isEditable: true,
      isEnabled: false,
      name: 'string',
      parentObjectTypeId: 7,
      pluginId: 10
    };
    describe('#filterPluginObjectTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterPluginObjectTypes(null, null, null, pluginsFilterPluginObjectTypesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(1, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(9, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'filterPluginObjectTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchemaForAllPluginObjectTypes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSchemaForAllPluginObjectTypes(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getSchemaForAllPluginObjectTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsUpdatePluginObjectTypeBodyParam = {
      extendedInfo: {},
      id: 4,
      isEditable: false,
      isEnabled: true,
      name: 'string',
      parentObjectTypeId: 8,
      pluginId: 3
    };
    describe('#updatePluginObjectType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePluginObjectType(555, pluginsUpdatePluginObjectTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'updatePluginObjectType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertCreateAlertForcedBodyParam = {
      alertFlowFalcon: {
        alertId: 7,
        directionId: 9,
        filterId: 7,
        id: 1,
        interfaceId: 5,
        netflowDeviceId: 8,
        viewId: 3
      },
      clearTime: 6,
      deviceId: 9,
      endTime: 7,
      id: 1,
      ignoreComment: 'string',
      ignoreUid: 9,
      ignoreUntil: 1,
      message: 'string',
      number: 2,
      objectId: 6,
      origin: 'system',
      pluginName: 'string',
      pollId: 2,
      severity: 8,
      startTime: 10,
      thresholdId: 2
    };
    describe('#createAlertForced - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAlertForced(alertCreateAlertForcedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.alertFlowFalcon);
                assert.equal(7, data.response.clearTime);
                assert.equal(6, data.response.deviceId);
                assert.equal(6, data.response.endTime);
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.ignoreComment);
                assert.equal(3, data.response.ignoreUid);
                assert.equal(6, data.response.ignoreUntil);
                assert.equal('string', data.response.message);
                assert.equal(6, data.response.number);
                assert.equal(7, data.response.componentId);
                assert.equal('trap', data.response.origin);
                assert.equal('string', data.response.pluginName);
                assert.equal(7, data.response.pollId);
                assert.equal(8, data.response.severity);
                assert.equal(9, data.response.startTime);
                assert.equal(8, data.response.thresholdId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alert', 'createAlertForced', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconDeviceAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconDeviceAlerts(555, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.alerts));
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.originIp);
                assert.equal(3, data.response.overrideName);
                assert.equal(7, data.response.peer);
                assert.equal('string', data.response.systemName);
                assert.equal('string', data.response.versions);
                assert.equal(5, data.response.visible);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alert', 'getFlowFalconDeviceAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertGetMaxSeverityAlertForObjectsBodyParam = [
      {
        deviceId: 5,
        objectId: 8
      }
    ];
    describe('#getMaxSeverityAlertForObjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMaxSeverityAlertForObjects(alertGetMaxSeverityAlertForObjectsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alert', 'getMaxSeverityAlertForObjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertPatchAlertBodyParam = {
      acknowledgedBy: 'string',
      alertFlowFalcon: {
        alertId: 10,
        directionId: 1,
        filterId: 1,
        id: 7,
        interfaceId: 8,
        netflowDeviceId: 5,
        viewId: 1
      },
      assignedTo: 7,
      clearMessage: 'string',
      clearTime: 2,
      closed: 5,
      closedKey: 2,
      comments: 'string',
      deviceId: 9,
      endTime: 1,
      id: 9,
      ignoreComment: 'string',
      ignoreUid: 7,
      ignoreUntil: 8,
      indicatorId: 10,
      isMaintenanceAlert: true,
      lastProcessed: 3,
      message: 'string',
      number: 4,
      objectId: 4,
      origin: 'flow',
      pluginName: 'string',
      pollId: 10,
      severity: 9,
      startTime: 3,
      thresholdId: 6
    };
    describe('#patchAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchAlert(555, alertPatchAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alert', 'patchAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimezonesByCountries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTimezonesByCountries((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CountriesAndTimezones', 'getTimezonesByCountries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicies(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(5, data.response.pageNumber);
                assert.equal(2, data.response.pageSize);
                assert.equal(6, data.response.totalElements);
                assert.equal(1, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesCreatePolicyBodyParam = {
      appendConditionMessages: false,
      clearConditions: [
        {
          aggregation: 10,
          comparison: 3,
          duration: 8,
          id: 4,
          indicatorTypeId: 1,
          isTrigger: true,
          message: 'string',
          policyId: 1,
          sigmaDirection: 6,
          type: 5,
          unit: 'string',
          value: 10
        }
      ],
      clearExpression: 'string',
      clearMessage: 'string',
      description: 'string',
      flow: {
        direction: 1,
        filterId: 4,
        id: 4,
        viewId: 9
      },
      folderId: 6,
      groupId: 9,
      groupIdList: [
        8
      ],
      id: 7,
      isDeviceGroup: true,
      isMemberOfAny: false,
      lastUpdated: 6,
      mailOnce: false,
      mailPeriod: 1,
      mailTo: 'string',
      name: 'string',
      objectSubTypeId: 8,
      objectTypeId: 6,
      pluginId: 2,
      severity: 4,
      triggerConditions: [
        {
          aggregation: 8,
          comparison: 4,
          duration: 9,
          id: 4,
          indicatorTypeId: 4,
          isTrigger: true,
          message: 'string',
          policyId: 6,
          sigmaDirection: 9,
          type: 8,
          unit: 'string',
          value: 4
        }
      ],
      triggerExpression: 'string',
      triggerMessage: 'string',
      type: 'other',
      useCustomTraps: false,
      useDefaultTraps: true,
      useDeviceTraps: false,
      userEnabled: 9
    };
    describe('#createPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicy(policiesCreatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.appendConditionMessages);
                assert.equal(true, Array.isArray(data.response.clearConditions));
                assert.equal('string', data.response.clearExpression);
                assert.equal('string', data.response.clearMessage);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.flow);
                assert.equal(7, data.response.folderId);
                assert.equal(1, data.response.groupId);
                assert.equal(true, Array.isArray(data.response.groupIdList));
                assert.equal(7, data.response.id);
                assert.equal(true, data.response.isDeviceGroup);
                assert.equal(false, data.response.isMemberOfAny);
                assert.equal(1, data.response.lastUpdated);
                assert.equal(false, data.response.mailOnce);
                assert.equal(7, data.response.mailPeriod);
                assert.equal('string', data.response.mailTo);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.objectSubTypeId);
                assert.equal(5, data.response.objectTypeId);
                assert.equal(8, data.response.pluginId);
                assert.equal(5, data.response.severity);
                assert.equal(true, Array.isArray(data.response.triggerConditions));
                assert.equal('string', data.response.triggerExpression);
                assert.equal('string', data.response.triggerMessage);
                assert.equal('flow', data.response.type);
                assert.equal(true, data.response.useCustomTraps);
                assert.equal(true, data.response.useDefaultTraps);
                assert.equal(false, data.response.useDeviceTraps);
                assert.equal(9, data.response.userEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'createPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesFilterPoliciesBodyParam = {
      description: 'string',
      folderId: 4,
      groupId: 3,
      ids: [
        10
      ],
      isDeviceGroup: true,
      name: 'string'
    };
    describe('#filterPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterPolicies(null, null, null, null, null, policiesFilterPoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(5, data.response.pageNumber);
                assert.equal(9, data.response.pageSize);
                assert.equal(4, data.response.totalElements);
                assert.equal(9, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'filterPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyFolders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyFolders(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(6, data.response.pageNumber);
                assert.equal(2, data.response.pageSize);
                assert.equal(7, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicyFolders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesCreatePolicyFolderBodyParam = {
      name: 'string',
      parentId: 9
    };
    describe('#createPolicyFolder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicyFolder(policiesCreatePolicyFolderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(1, data.response.parentId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'createPolicyFolder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesUpdatePolicyFolderBodyParam = {
      name: 'string'
    };
    describe('#updatePolicyFolder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicyFolder(555, policiesUpdatePolicyFolderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'updatePolicyFolder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyFolderById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePolicyFolderById(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deletePolicyFolderById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicy(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.appendConditionMessages);
                assert.equal(true, Array.isArray(data.response.clearConditions));
                assert.equal('string', data.response.clearExpression);
                assert.equal('string', data.response.clearMessage);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.flow);
                assert.equal(5, data.response.folderId);
                assert.equal(10, data.response.groupId);
                assert.equal(true, Array.isArray(data.response.groupIdList));
                assert.equal(6, data.response.id);
                assert.equal(true, data.response.isDeviceGroup);
                assert.equal(true, data.response.isMemberOfAny);
                assert.equal(3, data.response.lastUpdated);
                assert.equal(false, data.response.mailOnce);
                assert.equal(9, data.response.mailPeriod);
                assert.equal('string', data.response.mailTo);
                assert.equal('string', data.response.name);
                assert.equal(9, data.response.objectSubTypeId);
                assert.equal(10, data.response.objectTypeId);
                assert.equal(3, data.response.pluginId);
                assert.equal(3, data.response.severity);
                assert.equal(true, Array.isArray(data.response.triggerConditions));
                assert.equal('string', data.response.triggerExpression);
                assert.equal('string', data.response.triggerMessage);
                assert.equal('other', data.response.type);
                assert.equal(true, data.response.useCustomTraps);
                assert.equal(true, data.response.useDefaultTraps);
                assert.equal(true, data.response.useDeviceTraps);
                assert.equal(9, data.response.userEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesUpdatePolicyBodyParam = {
      appendConditionMessages: true,
      clearConditions: [
        {
          aggregation: 4,
          comparison: 4,
          duration: 3,
          id: 2,
          indicatorTypeId: 6,
          isTrigger: true,
          message: 'string',
          policyId: 4,
          sigmaDirection: 10,
          type: 4,
          unit: 'string',
          value: 2
        }
      ],
      clearExpression: 'string',
      clearMessage: 'string',
      description: 'string',
      flow: {
        direction: 8,
        filterId: 3,
        id: 2,
        viewId: 5
      },
      folderId: 7,
      groupId: 3,
      groupIdList: [
        10
      ],
      id: 10,
      isDeviceGroup: false,
      isMemberOfAny: true,
      lastUpdated: 3,
      mailOnce: true,
      mailPeriod: 5,
      mailTo: 'string',
      name: 'string',
      objectSubTypeId: 10,
      objectTypeId: 3,
      pluginId: 4,
      severity: 6,
      triggerConditions: [
        {
          aggregation: 7,
          comparison: 2,
          duration: 7,
          id: 9,
          indicatorTypeId: 9,
          isTrigger: true,
          message: 'string',
          policyId: 1,
          sigmaDirection: 5,
          type: 1,
          unit: 'string',
          value: 1
        }
      ],
      triggerExpression: 'string',
      triggerMessage: 'string',
      type: 'flow',
      useCustomTraps: true,
      useDefaultTraps: true,
      useDeviceTraps: false,
      userEnabled: 10
    };
    describe('#updatePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicy(555, policiesUpdatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'updatePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePolicyById(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deletePolicyById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActionsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getActionsUsingGET(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.mailAction);
                assert.equal('object', typeof data.response.trapAction);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getActionsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesUpdatePolicyActionBodyParam = {
      mailAction: {
        mails: [
          'string'
        ],
        repeated: false,
        repeatedPeriod: 8,
        roleIds: [
          4
        ],
        userIds: [
          7
        ]
      },
      trapAction: {
        trapDestinations: [
          7
        ],
        useCustomTraps: false,
        useDefaultTraps: false,
        useDeviceTraps: true
      }
    };
    describe('#updatePolicyAction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicyAction(555, policiesUpdatePolicyActionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'updatePolicyAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyActionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePolicyActionById(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deletePolicyActionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findAllUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findAllUsingGET(null, null, null, null, null, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(4, data.response.pageNumber);
                assert.equal(4, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(3, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'findAllUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesCreatePolicyConditionBodyParam = {
      aggregation: 3,
      comparison: 3,
      duration: 7,
      id: 2,
      indicatorTypeId: 2,
      isTrigger: true,
      message: 'string',
      policyId: 4,
      sigmaDirection: 1,
      type: 4,
      unit: 'string',
      value: 2
    };
    describe('#createPolicyCondition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicyCondition(555, policiesCreatePolicyConditionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.aggregation);
                assert.equal(9, data.response.comparison);
                assert.equal(3, data.response.duration);
                assert.equal(3, data.response.id);
                assert.equal(1, data.response.indicatorTypeId);
                assert.equal(true, data.response.isTrigger);
                assert.equal('string', data.response.message);
                assert.equal(7, data.response.policyId);
                assert.equal(3, data.response.sigmaDirection);
                assert.equal(1, data.response.type);
                assert.equal('string', data.response.unit);
                assert.equal(9, data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'createPolicyCondition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyConditionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyConditionById(555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.aggregation);
                assert.equal(10, data.response.comparison);
                assert.equal(5, data.response.duration);
                assert.equal(5, data.response.id);
                assert.equal(1, data.response.indicatorTypeId);
                assert.equal(false, data.response.isTrigger);
                assert.equal('string', data.response.message);
                assert.equal(3, data.response.policyId);
                assert.equal(7, data.response.sigmaDirection);
                assert.equal(1, data.response.type);
                assert.equal('string', data.response.unit);
                assert.equal(9, data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicyConditionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesUpdatePolicyConditionBodyParam = {
      aggregation: 7,
      comparison: 9,
      duration: 6,
      id: 7,
      indicatorTypeId: 6,
      isTrigger: true,
      message: 'string',
      policyId: 4,
      sigmaDirection: 1,
      type: 4,
      unit: 'string',
      value: 2
    };
    describe('#updatePolicyCondition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicyCondition(555, 555, policiesUpdatePolicyConditionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'updatePolicyCondition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyConditionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePolicyConditionById(555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deletePolicyConditionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllReports - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllReports(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(8, data.response.pageNumber);
                assert.equal(7, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getAllReports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportsCreateReportBodyParam = {
      description: 'string',
      emailTo: 'string',
      folderId: 9,
      id: 8,
      isEmailed: false,
      isFtped: false,
      isPrivate: false,
      isReadOnly: false,
      isTemporary: true,
      lastAccessed: 6,
      lastModified: 9,
      message: 'string',
      migrated: 4,
      name: 'string',
      nextRun: 5,
      numberOfColumns: 9,
      runUnit: 'hours',
      runValue: 6,
      templateType: 'object',
      timezone: 'string',
      userId: 3,
      userRoleId: 6
    };
    describe('#createReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createReport(reportsCreateReportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.emailTo);
                assert.equal(8, data.response.folderId);
                assert.equal(1, data.response.id);
                assert.equal(false, data.response.isEmailed);
                assert.equal(false, data.response.isFtped);
                assert.equal(false, data.response.isPrivate);
                assert.equal(true, data.response.isReadOnly);
                assert.equal(false, data.response.isTemporary);
                assert.equal(6, data.response.lastAccessed);
                assert.equal(9, data.response.lastModified);
                assert.equal('string', data.response.message);
                assert.equal(1, data.response.migrated);
                assert.equal('string', data.response.name);
                assert.equal(6, data.response.nextRun);
                assert.equal(1, data.response.numberOfColumns);
                assert.equal('string', data.response.runUnit);
                assert.equal(6, data.response.runValue);
                assert.equal('string', data.response.templateType);
                assert.equal('string', data.response.timezone);
                assert.equal(1, data.response.userId);
                assert.equal(5, data.response.userRoleId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'createReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllReportFolders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllReportFolders(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(5, data.response.pageNumber);
                assert.equal(9, data.response.pageSize);
                assert.equal(2, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getAllReportFolders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportsCreateReportFolderBodyParam = {
      id: 1,
      name: 'string',
      parentId: 10
    };
    describe('#createReportFolder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createReportFolder(reportsCreateReportFolderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(9, data.response.parentId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'createReportFolder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportsUpdateReportFolderByIdBodyParam = {
      id: 2,
      name: 'string',
      parentId: 4
    };
    describe('#updateReportFolderById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateReportFolderById(555, reportsUpdateReportFolderByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'updateReportFolderById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReportFolderById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteReportFolderById(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'deleteReportFolderById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReport(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.emailTo);
                assert.equal(8, data.response.folderId);
                assert.equal(8, data.response.id);
                assert.equal(false, data.response.isEmailed);
                assert.equal(false, data.response.isFtped);
                assert.equal(true, data.response.isPrivate);
                assert.equal(true, data.response.isReadOnly);
                assert.equal(false, data.response.isTemporary);
                assert.equal(10, data.response.lastAccessed);
                assert.equal(10, data.response.lastModified);
                assert.equal('string', data.response.message);
                assert.equal(1, data.response.migrated);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.nextRun);
                assert.equal(8, data.response.numberOfColumns);
                assert.equal('string', data.response.runUnit);
                assert.equal(3, data.response.runValue);
                assert.equal('string', data.response.templateType);
                assert.equal('string', data.response.timezone);
                assert.equal(6, data.response.userId);
                assert.equal(7, data.response.userRoleId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportsUpdateReportByIdBodyParam = {
      description: 'string',
      emailTo: 'string',
      folderId: 7,
      id: 1,
      isEmailed: false,
      isFtped: true,
      isPrivate: false,
      isReadOnly: true,
      isTemporary: true,
      lastAccessed: 1,
      lastModified: 6,
      message: 'string',
      migrated: 6,
      name: 'string',
      nextRun: 4,
      numberOfColumns: 4,
      runUnit: 'hours',
      runValue: 9,
      templateType: 'device',
      timezone: 'string',
      userId: 2,
      userRoleId: 9
    };
    describe('#updateReportById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateReportById(555, reportsUpdateReportByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'updateReportById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReportById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteReportById(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'deleteReportById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachmentAggregation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTelephonyAttachmentAggregation(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('DestinationIP', data.response.aggregation);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'getTelephonyAttachmentAggregation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTelephonyUpdateTelephonyAttachmentAggregationBodyParam = {
      aggregation: 'Source'
    };
    describe('#updateTelephonyAttachmentAggregation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTelephonyAttachmentAggregation(555, reportAttachmentsTelephonyUpdateTelephonyAttachmentAggregationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'updateTelephonyAttachmentAggregation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTelephonyAttachmentSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resultLimitSetting);
                assert.equal('object', typeof data.response.telephonySetting);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'getTelephonyAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTelephonyUpdateTelephonyAttachmentSettingsBodyParam = {
      resultLimitSetting: {
        resultLimit: 10
      },
      telephonySetting: {
        sortBy: 'r',
        sortOrder: 'DESC'
      }
    };
    describe('#updateTelephonyAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTelephonyAttachmentSettings(555, reportAttachmentsTelephonyUpdateTelephonyAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'updateTelephonyAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTelephonyAttachmentTimeSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ranges);
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'getTelephonyAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTelephonyUpdateTelephonyAttachmentTimeSettingsBodyParam = {
      ranges: {
        end: 'string',
        start: 'string',
        type: 'CustomWeek'
      },
      timezone: 'string'
    };
    describe('#updateTelephonyAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTelephonyAttachmentTimeSettings(555, reportAttachmentsTelephonyUpdateTelephonyAttachmentTimeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'updateTelephonyAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTelephonyAttachmentVisualizationSettings(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'getTelephonyAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTelephonyUpdateTelephonyAttachmentVisualizationSettingsBodyParam = {
      table: {
        columnSetting: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        csvSetting: {
          expandTimeseries: false,
          formatTimestamp: true
        },
        dataPresentationSetting: {
          formatNumbers: false,
          precision: 2
        },
        tableSetting: {
          blockVis: false,
          hideHeaders: false,
          wrapCells: true
        }
      }
    };
    describe('#updateTelephonyAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTelephonyAttachmentVisualizationSettings(555, reportAttachmentsTelephonyUpdateTelephonyAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'updateTelephonyAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTelephonyCreateTelephonyAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createTelephonyAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTelephonyAttachment(555, reportAttachmentsTelephonyCreateTelephonyAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.timeSettings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'createTelephonyAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dynamicPluginRegisterDynamicPluginManagerBodyParam = {
      pluginManagerName: 'string',
      pluginManagerIdentifier: 'string'
    };
    describe('#registerDynamicPluginManager - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.registerDynamicPluginManager(dynamicPluginRegisterDynamicPluginManagerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.identifier);
                assert.equal(1, data.response.lastSeen);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DynamicPlugin', 'registerDynamicPluginManager', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dynamicPluginRegisterDynamicPluginBodyParam = {
      fields: [
        {
          defaultEntry: 'string',
          description: 'string',
          enumList: 'string',
          maxLength: 3,
          name: 'string',
          type: 'TEXT',
          valueRange: 'string'
        }
      ],
      name: 'string',
      technology: 'string'
    };
    describe('#registerDynamicPlugin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.registerDynamicPlugin(555, dynamicPluginRegisterDynamicPluginBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.fields));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.technology);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DynamicPlugin', 'registerDynamicPlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceComponentGroupPartiallyUpdateObjectGroupByIdBodyParam = {
      name: 'string',
      parentId: 10
    };
    describe('#partiallyUpdateObjectGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateObjectGroupById(555, deviceComponentGroupPartiallyUpdateObjectGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceComponentGroup', 'partiallyUpdateObjectGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicePartiallyUpdateDeviceByIdBodyParam = {
      name: 'string',
      alternateName: 'string',
      description: 'string',
      ipAddress: 'string',
      pollFrequency: 5,
      allowDelete: false,
      disablePolling: false,
      disableConcurrentPolling: true,
      disableThresholding: false,
      timezone: 'string',
      workhoursGroupId: 9,
      objects: [
        {
          id: 5,
          name: 'string',
          alternateName: 'string',
          description: 'string',
          pluginId: 9,
          pluginObjectTypeId: 7,
          pollFrequency: 5,
          subtypeId: 3,
          extendedInfo: {},
          enabled: 'ENABLED',
          isDeleted: true,
          isEnabled: false,
          isVisible: false,
          indicators: [
            {
              id: 4,
              evaluationOrder: 9,
              format: 'COUNTER64',
              isBaselining: true,
              lastInvalidationTime: 1,
              maxValue: 10,
              pluginIndicatorTypeId: 5,
              syntheticExpression: 'string',
              systemMaxValue: 9,
              extendedInfo: {},
              isDeleted: true,
              isEnabled: true
            }
          ]
        }
      ],
      pluginInfo: {}
    };
    describe('#partiallyUpdateDeviceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateDeviceById(555, devicePartiallyUpdateDeviceByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'partiallyUpdateDeviceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapImages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMapImages(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(2, data.response.totalElements);
                assert.equal(5, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMapImages', 'getMapImages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapImageById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMapImageById(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMapImages', 'getMapImageById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPostLoggingLevelBodyParam = {
      level: 'OFF',
      logger: 'string'
    };
    describe('#postLoggingLevel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postLoggingLevel(applicationPostLoggingLevelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal(100, data.response.statusCode);
                assert.equal(9, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'postLoggingLevel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPublicMetrics - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPublicMetrics(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getPublicMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVersion((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.buildTime);
                assert.equal('string', data.response.buildVersion);
                assert.equal('string', data.response.commitIdAbbrev);
                assert.equal('string', data.response.commitTime);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupsRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupsRules(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(7, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(1, data.response.totalElements);
                assert.equal(8, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'getDeviceGroupsRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupRulesCreateDeviceGroupRuleBodyParam = {
      attributeId: 5,
      descriptionExpression: 'string',
      groupId: 10,
      id: 10,
      metadataValueExpression: 'string',
      mgtIpExpression: 'string',
      nameExpression: 'string',
      namespaceId: 3,
      sysContactExpression: 'string',
      sysDescrExpression: 'string',
      sysLocationExpression: 'string',
      sysNameExpression: 'string',
      sysObjectIdExpression: 'string',
      walkCheckOid: 'string'
    };
    describe('#createDeviceGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceGroupRule(deviceGroupRulesCreateDeviceGroupRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.attributeId);
                assert.equal('string', data.response.descriptionExpression);
                assert.equal(7, data.response.groupId);
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.metadataValueExpression);
                assert.equal('string', data.response.mgtIpExpression);
                assert.equal('string', data.response.nameExpression);
                assert.equal(7, data.response.namespaceId);
                assert.equal('string', data.response.sysContactExpression);
                assert.equal('string', data.response.sysDescrExpression);
                assert.equal('string', data.response.sysLocationExpression);
                assert.equal('string', data.response.sysNameExpression);
                assert.equal('string', data.response.sysObjectIdExpression);
                assert.equal('string', data.response.walkCheckOid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'createDeviceGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupRule(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.attributeId);
                assert.equal('string', data.response.descriptionExpression);
                assert.equal(1, data.response.groupId);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.metadataValueExpression);
                assert.equal('string', data.response.mgtIpExpression);
                assert.equal('string', data.response.nameExpression);
                assert.equal(4, data.response.namespaceId);
                assert.equal('string', data.response.sysContactExpression);
                assert.equal('string', data.response.sysDescrExpression);
                assert.equal('string', data.response.sysLocationExpression);
                assert.equal('string', data.response.sysNameExpression);
                assert.equal('string', data.response.sysObjectIdExpression);
                assert.equal('string', data.response.walkCheckOid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'getDeviceGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupRulesUpdateDeviceGroupRuleBodyParam = {
      attributeId: 2,
      descriptionExpression: 'string',
      groupId: 10,
      id: 10,
      metadataValueExpression: 'string',
      mgtIpExpression: 'string',
      nameExpression: 'string',
      namespaceId: 10,
      sysContactExpression: 'string',
      sysDescrExpression: 'string',
      sysLocationExpression: 'string',
      sysNameExpression: 'string',
      sysObjectIdExpression: 'string',
      walkCheckOid: 'string'
    };
    describe('#updateDeviceGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupRule(555, deviceGroupRulesUpdateDeviceGroupRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'updateDeviceGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceGroupRule(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'deleteDeviceGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupRules(555, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(10, data.response.pageNumber);
                assert.equal(7, data.response.pageSize);
                assert.equal(3, data.response.totalElements);
                assert.equal(10, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'getDeviceGroupRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
